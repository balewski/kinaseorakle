#!/usr/bin/env python
# -u     : unbuffered stdout and stderr, launch it via $ python -u runXX.py
from __future__ import print_function
""" read input hd5 tensors + protein dictionary
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Kinase import Plotter_Kinase
from Deep_Kinase import Deep_Kinase

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Perform  Kinase Oracle training',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
 
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--seedModel",
                        default=None,
                        help="seed model and weights")

    parser.add_argument("--seedWeights",
                        default=None,
                        help="seed weights only, after model is loaded, 'same' is allowed")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument( "-t","--timeline", dest='tf_timeline', action='store_true',
                         default=False,help="TF will accumulate timline, usefull for profiling")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable Xterm for batch mode")
    parser.add_argument('--venue', dest='venue', choices=['web','icml'],
                         default='web',
                         help="change fromat of output plots to match publisher requirements")

    parser.add_argument( "--noPlot", dest='noPlot', action='store_true',
                          default=False,help="disable any plotting, no matplotlib-TNP not used yet")

    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="num epochs")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=128,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=512*8,
                        help="num HK-RR pairs for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument("--noiseFract", type=float,
                        default=0.1,
                        help="enables masking of amino acids for oversampled data")

    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=5,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLr", dest='reduceLearn',
                         action='store_true',default=False,help="reduce learning at plateau")

    args = parser.parse_args()
    args.prjVersion='10'
    args.prjName='kinase'+args.prjVersion # core name used everywhere

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_Kinase(args )
deep=Deep_Kinase(args)

deep.load_taxonomy()
deep.load_proteins_fasta('HK')
deep.load_proteins_fasta('RR')

bala='over'

if  args.epochs>0 :
    deep.load_labeled_pairs_yaml('val',bala)
    deep.load_labeled_pairs_yaml('train',bala)
deep.load_labeled_pairs_yaml('val','natur')

deep.clamp_pad_1hot_pairs(args)

if args.seedModel==None:
    print('start fresh model')
    deep.build_compile_model(args,bala)
else:
    deep.load_compile_1model(path=args.seedModel)

ppp.plot_model(deep)

if args.seedWeights!=None:
    if args.seedWeights=='same':
        deep.load_weights_only(args.seedModel) #never used
    else:
        deep.load_weights_4_retrain(path=args.seedWeights)

if args.epochs >3:  deep.save_model() 

if args.epochs>0:
    deep.train_model(args,bala,bala) # train & validation balances 
    deep.save_model() 
    ppp.plot_train_history(deep,args,10)
    ppp.plot_ROC('val_'+bala,deep,args,10)

figId=11
nEve=12000
if args.venue!='web': neve=1000000
ppp.plot_ROC('val_natur',deep,args,figId,nEve)
if args.venue!='web': figId=12
ppp.plot_labeledScores('val_natur',deep,args,figId)

ppp.pause(args,'train') 

