#!/usr/bin/env python
""" 
read yaml & png  predictions for a species and publish it on the web
copy files and creates  cover page with summary and taxonomy classes 

Size & file count:  600 species & givenRR : 2.7 GB, 21k files, takes 90 sec to copy this tree
Estimate for all 1.6k species x (HK,RR) ==> faxt 5.3 --> 14GB, 111k files  

Target plots & web-tree already exist
/givenRR/
  -specName1/
     summary.yml,  a1.png, b1.png, ..., all plots  
  -specName2/ ....

/givenHK/
  -specName1/ ...

/data/
   specName1.matrix.yml, specName2.matrix.yml, ...

Writes html & class-plots to: 
/givenRR/species and /givenHK/species/ 

Action (no data are moved):
a) find all given[RR/HK]/specN/summary.yml  files  
b) loop over species, subdivide them on to classes, create class-summary page+plots
c) create overall summary page

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


from PubAll_Kinase import PubAll_Kinase

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 

    parser.add_argument('-g',"--given", choices=['HK','RR'],
                        default='RR',help="fixed protein role")

    parser.add_argument("--dataPath",
                        default='/project/projectdirs/m2925/www/tmp/kinase10e-plots/',help="input/output path")
    
    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument('--venue', dest='venue', choices=['web','icml'],
                         default='web',
                         help="change fromat of output plots to match publisher requirements")

    args = parser.parse_args()
    args.prjVersion='10'
    args.prjName='kinase'+args.prjVersion # core name used everywhere
    args.deepName='Kinase Oracle '+args.prjVersion
    txt=args.dataPath.replace('projectdirs/','')
    txt=txt.replace('www/','')
    #args.webPath='portal.nersc.gov'+txt
    args.webPath=txt

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

#?specNameL=['Escherichia_coli_536']

#?pub=PubAll_Kinase(args ,specNameL=specNameL[:])
#pub=PubAll_Kinase(args , maxSpec=50)
pub=PubAll_Kinase(args )
pub.processTaxons()
pub.coverHTML()
print('done, URL: http://portal.nersc.gov/%s'%pub.webPath)

