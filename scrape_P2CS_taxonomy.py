import scrapy

#$ scrapy runspider p2cs_dumpSpec.py 

class QuotesSpider(scrapy.Spider):
    name = "dump_all_spec"
    allowed_domains = ["p2cs.org"] # only follow links for this domains
    start_urls = ['http://www.p2cs.org/index.php?section=browse&PHPSESSID'] # this will be the 1st domain visitted after execution

    def parse(self, response):  # call back called after URL returns
        self.log('JAN just visttted:'+response.url)
        tabA=response.css('div.doc > table > tr > td')
        #print('JAN2:', len(tabA))
        tabB=tabA.css('td.TextMutant_liste')

        #tabC=tabB.xpath('.//a[contains(@href, td)]') # has 3854 elements
        
        fd = open("tab.html2","w") 
 
        fd.write("<table>\n") 
        for x in tabB.extract():
            if 'href' not in x: continue
            
            fd.write("%s\n"%x) 
            #break
        fd.close()
