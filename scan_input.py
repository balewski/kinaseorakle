#!/usr/bin/env python
""" 
various auxiliary characterization of input data
for kinase Oracle
"""
import numpy as np
import os, time
import shutil
import datetime

import matplotlib.pyplot as plt

from Oracle_Kinase import Oracle_Kinase
#from matplotlib.colors import LogNorm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='input data characterization for Kinase Oracle',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='kinase7',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument("--dataPath",
                        default='data',help="path to input")

#    parser.add_argument("--inpPath",
#                        default='out',help="path to input")

#    parser.add_argument("--plotPath",
#                        default='plot',help="path to out plots")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots/output")
    

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


from Util_Kinase import getGeneAddress as getGeneAddress
from pprint import pprint

def get_pair_separation(ora,specN):
    assert specN in ora.coll
    coll=ora.coll[specN]
    #print('vv',list(coll))
    anyD={}
    for role in ora.mr12:
        dd={ x:getGeneAddress(x) for x in coll[role] }
        anyD.update(dd)

    dd=anyD
    anyL=[ x  for x in sorted(dd, key=dd.get, reverse=False)]
    #pprint(anyL)
    
    inpD=coll['HK']
    out=[]
    nPair=0
    for protN in inpD:
        if 'pair' not in inpD[protN] : continue
        nPair+=1
        idxHK=anyL.index(protN)
        idxRR=anyL.index(inpD[protN]['pair'])
        idxDist=idxHK-idxRR
        out.append(idxDist)
        if abs(idxDist)>4 :
            print(nPair,protN,idxHK,idxRR,idxDist)
        #assert abs(idxDist)<5
    return out


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ora=Oracle_Kinase(args)
#ppp=Plotter_Kinase(args )

ora.load_taxonomy()
ora.load_proteins_fasta('HK')
ora.load_proteins_fasta('RR')


specD=sorted(ora.taxonDB.keys())[::1]

sepL=[]
for specN in specD:
    ora.expand_species(specN)
    nPair=ora.count_pairs(specN)
    print(specN,' nPair=',nPair)
    if nPair<2 : continue

    sepL+=get_pair_separation(ora,specN)

print('len2:',len(sepL))

plt.figure(10,facecolor='white', figsize=(7,4))
ax=plt.subplot(1,1, 1)

bins = np.linspace(-15.5, 15.5, 32)
print(bins)
ax.hist(sepL, bins, alpha=0.5)
ax.set_yscale('log')
ax.set(xlabel='HKpos - RRpos', ylabel='pairs')
ax.grid(True)
plt.tight_layout()
plt.show()
