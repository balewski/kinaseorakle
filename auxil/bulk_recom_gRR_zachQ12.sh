#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

plotPath=/project/projectdirs/m2925/www/tmp/plots_tax1

scorePath=./scores_tax1

echo make-web-dirs ${plotPath}
mkdir -p $plotPath/givenRR/taxons
mkdir -p $plotPath/givenHK/taxons
mkdir -p $plotPath/data

chmod a+x -R $plotPath 
chmod a+r -R $plotPath 

k=0
for ymlN in $( ls $scorePath)  ; do
    #echo $ymlN
    specN=${ymlN:0:-17}
    k=$[ $k + 1]
    echo    
    echo " ==================  work on $k specN=$specN   $scorePath  "
    ./recomOne_Kinase.py --scorePath $scorePath  --specName $specN --plotPath $plotPath  -X 

    #break
    #if [ $k -gt 5 ] ; then  exit ; fi

done

exit
 ==================   
./recomOne_Kinase.py --specName Acidimicrobium_ferrooxidans_DSM_10331   --scorePath scores --plotPath plots --given RR


if [ -d old-${plotPath} ]; then
    rm -rf old-${plotPath}
fi
