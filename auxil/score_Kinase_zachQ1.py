#!/usr/bin/env python
""" 
Zach asked Q1:   "Which residues do the NN deem important for the interaction?"

 We change it to '0' - meaning 'not important'.
The pseudo code will be:

scoreBest=score(HK,RR)
zL=[]
for i in len(RR):
	RRi=RR(i='X')
        y=score(HK,RRi)
        z.append(y-score-best)
u=std_dev(z)


 predict best HK-RR pairs for given species
 uses a series of pre trained models (kfolds), 
 run prediction with each model, then saves all scores
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import yaml
from Oracle_Kinase import Oracle_Kinase

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='recommend best HK-RR pairs for given species',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='kinase10',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument("--dataPath",
                        default='data',help="path to input")
    parser.add_argument("--outPath",
                        default='scores/',help="output path for plots/output")
    
    parser.add_argument("-l", "--bulkSpecList", type=int, default=None,
                        help="many species from spec-split.yml, segment=l")

    parser.add_argument("--seedModel",default='fixMe4',
                        help="trained model and weights")

    parser.add_argument("-k","--kModelList",nargs="+",
                        default=['10'],
                        help=" blank separated list of kfold IDs, takes n1-n2")

    parser.add_argument("--seedWeights",
                        default='none',
                        help="seed weights only, after model is loaded")
    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.events=0
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    #extension:
    
    if len(args.kModelList[0])>0 :
        args.seedModel='/global/homes/b/balewski/prj/kinase10e_all/kinase10e-'
        #args.seedModel='/global/cscratch1/sd/balewski/kinase10_sum/10e/kinase10e-'

    return args

from Util_Kinase import excludeL_zach as excludeL_zach
specD=excludeL_zach


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

specN="Escherichia coli 536" # drop '_' from names
varHKname='ECP_2164_HK_Classic'

if args.bulkSpecList!=None : # activate loading large list of species
    listName=args.dataPath+'/'+args.prjName+'.spec-split.yml'
    print('load seg=%d from %s'%(args.bulkSpecList,listName))
    ymlf = open(listName, 'r')
    bulk=yaml.load( ymlf)
    ymlf.close()
    assert args.bulkSpecList in bulk
    specD=bulk[args.bulkSpecList]
    #specD=specD[::10] # for 10% of data

print('recommend HK-RR pairs for specL len=',len(specD))

ora=Oracle_Kinase(args)

ora.load_taxonomy()
ora.load_proteins_fasta('HK')
ora.load_proteins_fasta('RR')

myHK=ora.protDB['HK'][varHKname]
hk_seq=myHK['seq']
hk_len=len(hk_seq)
print(' found varHK',myHK)

ora.load_Kmodels(path=args.seedModel,kL=args.kModelList)

if args.seedWeights!='none':
    assert len(args.kModelList)==1
    ora.load_weights_only(path=args.seedWeights)

d=4
brick=ora.padChar*d
for k in range(0,hk_len-d,d):
    ora.coll={}  # clear previous
    ora.expand_species(specN) 
    ora.build_pairs_array() # any HK against any RR
    new_seq=hk_seq[:k]+brick+hk_seq[k+d+1:]
    print('org HK',hk_seq[:60])
    print('new HK',new_seq[:60], len(new_seq),k)
    ora.protDB['HK'][varHKname]['seq']=new_seq
    ora.clamp_pad_1hot_pairs(args)
    # expand species name by adding k as suffix
    ora.coll={specN+'_d%dk%02d'%(d,k):  ora.coll[specN]}
    ora.predict_one_species()
    print('done with %d  : %s\n'%(k,specN))
print('recommend -done')
exit(1)

if 0:
    specD=sorted(ora.taxonDB.keys())[::10]
    specD2=[]
    for specN in specD:
        ora.expand_species(specN)
        nPair=ora.count_pairs(specN)
        if nPair<=30: continue
        specD2.append(specN)
        print(len(specD2),specN,nPair)
        print("   '%s', # pairs=%d"%(specN,nPair))
        specD=specD2


