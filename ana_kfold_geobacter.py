#!/usr/bin/env python
""" 
agregate kfold analysis for kinase Oracle
read yaml kfold_one summaries,
"""
import numpy as np
import os, time
import shutil
import datetime
import yaml

#from matplotlib.colors import LogNorm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='kinase7',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("--inpPath",
                        default='out',help="path to input")

    parser.add_argument("--plotPath",
                        default='plot',help="path to out plots")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Ana_Kfold(object):

    def __init__(self, args,nKF): 
        self.verb=args.verb
        self.kfold={} # [idx] [input full yaml records]
        self.topK={} # [rr][score],  merged top k  from all kfolds
        self.trueHK={} # [rr] [ orpan or true HK]

        allL=os.listdir(args.inpPath)
        idxL=[]
        for x in allL:
            if 'yml'  in x: continue
            if int(x[3:]) <40 : continue #use only idx[40-59] to smaple all segments
            idxL.append(x)
            if nKF>0 and len(idxL)>=nKF: break
        print('found %d idx-kfolds'%len(idxL))
        assert len(idxL) >0


        for x in sorted(idxL):
            path=args.inpPath+'/'+x
            y=os.listdir(path)[0]
            assert 'yml' in y
            self.read_one(x,path+'/'+y)
        #print(self.kfold['val_natur'])

        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')

        self.plt=plt
        self.figL=[]
        self.pltName=[]

    #............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/%s_f%d'%(args.plotPath,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()


    #............................
    def read_one(self,key,inpF):
        print('load kfold from',inpF)
        ymlf = open(inpF, 'r')
        inpD=yaml.load( ymlf)
        ymlf.close()
        if len(self.kfold)<1:
            print('   found %d records'%len(inpD), list(inpD));
        assert len(inpD) >0        
        self.kfold[key]=inpD

    #............................
    def merge_topK(self,rr,K=4):
        self.topK[rr]={}
        self.trueHK[rr]=set()
        for kf in self.kfold:
            one=self.kfold[kf]
            self.topK_one_Kfold(rr,one['matrix'],self.topK[rr],K)
            print(' after kf=%s  HK-size topK=%d'%(kf,len(self.topK[rr])) )


    #............................
    def topK_one_Kfold(self,rr,inpD,topK,K):
        #print('ii',rr,list(inpD[rr]))
        try:
            trueHK=inpD[rr]['truth']['pair']
            self.trueHK[rr].add(trueHK)
        except:
            trueHK=inpD[rr]['truth']

        dd=inpD[rr]['pred']
        topK_L=[ (x, dd[x] ) for x in sorted(dd, key=dd.get, reverse=True)]
        
        print('\n- - - - - -  sorted matches for RR=',rr)
        nHK=min(K,len(topK_L))
        for i in range(nHK):
            hk,score= topK_L[i]
            if hk  not in  topK : topK[hk]=0.
            #topK[hk]+=score
            topK[hk]+=1
            trStr=''
            if trueHK==hk :  trStr=' TRUTH' 
            print('rank=%d %s  score=%.4g %s'%(i,hk,score,trStr))
        
    #............................
    def show_best(self,rr,K):
        trueHK='orphan'
        if len(self.trueHK[rr]) : trueHK=self.trueHK[rr].pop()
        print('\n= = = = = = best top-%d out of %d for %s --> %s'%(K,len(self.topK[rr]),rr,trueHK))
        
        dd=self.topK[rr]
        topK_L=[ (x, dd[x] ) for x in sorted(dd, key=dd.get, reverse=True)]
        
        nHK=min(K,len(topK_L))
        for i in range(nHK):
            hk,score= topK_L[i]
            trStr=''
            if trueHK==hk : trStr=' TRUTH'
            print('rank=%d %s  scoreSum=%.4g %s'%(i,hk,score,trStr))
        

    #............................
    def plot_one(self,segName,obsN,symCol):
        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,3))
        nrow,ncol=1,5
        sym,col=symCol
        
        inp=self.kfold[segName]
        vK=[];  vY=[] 
        for rec in inp:
            k=rec['kfold']
            vK.append(k)
            if obsN=='balance neg/pos':
                vY.append(rec['nNeg']/rec['nPos'])
            else:
                vY.append(rec[obsN])

        if obsN=='auc': obsN='AUC' #tmp
        
        ax=self.plt.subplot(nrow,ncol,1)
        bp=ax.boxplot(vY, 0,'')
        #self.plt.setp(ax.get_xticklabels() , visible=False)
        #ax.set_xticks([])
        
        #print('bp',bp,type(bp))
        mm=bp['medians'][0].get_ydata()[0]
        #print('mm',mm)
        ax.set(ylabel=obsN, xlabel=segName,title='%s=%.2f'%(obsN,mm))
        ax.yaxis.tick_right()
        ax.set_xticks([])
        

        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=ncol-1, sharey=ax )
        ax.plot(vK, vY,symCol, markersize=10);
        ax.set(xlabel='kfold index', title='%s for segment_balance=%s'%(obsN, segName))
        ax.grid(axis='y')
        #ax.set_yticklabels([])
        self.plt.setp(ax.get_yticklabels() , visible=False)
        ax.set_ylim(0.97,.99)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ana=Ana_Kfold(args,0)

rrL={'GSU1250_RR_NtrC'}  # known truth
rrL={'GSU1658_RR_PleD'}  #4 Zach

K=8

for rr in rrL:
    ana.merge_topK(rr,K)
    ana.show_best(rr,K)
    #ppp.plot_one('test_under',x,obsL[x])
#ppp.pause(args,'eval')
