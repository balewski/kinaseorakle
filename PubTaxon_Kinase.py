import os, datetime
import numpy as np
from matplotlib.colors import LogNorm
from matplotlib import cm as cmap


__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


#............................
#............................
#............................
class PubTaxon_Kinase(object):

    def __init__(self, args,taxonN,dataPath):
        self.venue=args.venue
        self.imgForm='png'
        if args.venue!='web' :  self.imgForm='pdf'

        self.name=taxonN
        self.dataPath=dataPath+'/taxons/'
        self.given=args.given
        self.recom='HK'
        if self.given=='HK':self.recom='RR'

        self.tableColumnNames=['Species','HK','RR','known pairs','true%s inTop2'%self.recom,'true%s inTop4'%self.recom,'true%s inTop8'%self.recom,'avrTrue rank','AUC']
        self.specD={} # [specN] - plain dict of all species

        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started for',self.name)

        self.plt=plt
        self.figL=[]
        self.pltName=[]

    #............................
    def numSpec(self): return len(self.specD)

    #............................
    def add_species(self,bulkD):
        specN=bulkD['Species']
        self.specD[specN]=bulkD

   #............................
    def process(self): 
        self.makePlots()
        self.makeHTML()

    #----------------
    def makePlots(self):
        print('plots for taxon=',self.name)
        v1=[]; # coinces count
        v1a=[];        v1b=[]; v1c=[]
        v2=[] ; v3=[];v4=[]

        top4N='true%s inTop4'%self.recom
        for specN in self.specD:
            #print('oo specN=',specN)
            rec=self.specD[specN]
            nChoice=rec[self.recom]
            knownPairs=rec['known pairs']
            avrTR=rec['avrTrue rank'].split()
            top4=rec[top4N]

            v1.append(nChoice)

            if knownPairs>5 :
                v1c.append(nChoice)
                v4.append(top4/knownPairs)

            if avrTR[0] != 'n/a':
                v1b.append(nChoice)
                v3.append(float(avrTR[0]))
            auc=rec['AUC']            
            if auc != 'n/a':
                v2.append(float(auc))
                v1a.append(nChoice)

        print('v1a',len(v1a),'v1b',len(v1b))
        #print(v1b)
        #print(v4)

        mxx=max(v1)

        self.plot_AUC_2d(v1a,v2,mxx,31)
        self.plot_avrTR_2d(v1b,v3,mxx,32)
        self.plot_fracTop4_2d(v1c,v4,mxx,33)

    
    #......................
    def plot_AUC_2d(self,vx,vy,mxx,figId):
        if len(vx)<2: return
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4))
        ax=self.plt.subplot(1,1, 1)

        myRange=np.array([(0,mxx), (0.9,1.)])
        hb = ax.hist2d(vx,vy , bins=20, range=myRange,  cmin=1, cmap=cmap.rainbow)
        ax.set(xlabel='num %s choices per species'%(self.recom) ,ylabel='AUC of ROC')
        if self.venue=='web' :   ax.set_title('%d %s'%(len(vx),self.name) ) 
            
        cb = fig.colorbar(hb[3], ax=ax)
        cb.set_label('spec count')
        ax.grid(color='brown', linestyle='--')
        ax.set_ylim(0.9,1.)
       
        self.plt.tight_layout()

        coreFig='fig%d_taxon_%s'%(figId,self.name)
        self.save_plot( coreFig )
        self.pltName.append((coreFig,'AUC vs. choice count'))

    #......................
    def plot_avrTR_2d(self,vx,vy,mxx,figId):
        if len(vx)<2: return
        xIn=4.3
        
        fig=self.plt.figure(figId,facecolor='white', figsize=(xIn,xIn/1.3))

        ax=self.plt.subplot(1,1, 1)
        myRange=np.array([(0,mxx), (0,6)])

        hb = ax.hist2d(vx,vy , bins=20, range=myRange, cmin=1, cmap=cmap.cool)

        ax.set(xlabel='num %s choices per species'%(self.recom) ,ylabel='average rank of true %s'%self.recom)

        vyA=np.array(vy)
        avrY=np.average(vyA)
        stdY=np.std(vyA)
        avrStr='%.1f +/- %.1f'%(avrY,stdY)
        self.avrStr=avrStr
        #print('figId=%d %s'%(figId,avrStr) )
        #name = sorted(self.pltName)[0]
        #print('avrX : %s     &  $ %.1f \\pm %.1f$'%(name,avrY,stdY))
        
        if self.venue=='web' :   
            ax.set_title('%d %s'%(len(vx),self.name) ) 
            ax.grid() #color='brown', linestyle='--')
            ax.axhline(stdY, linestyle='--')
            ax.text(20,3.1,'avr='+avrStr)

        cb = fig.colorbar(hb[3], ax=ax,format='%.0f')
        cb.set_label('species count')

        #ax.set_ylim(0.,10.)
        self.plt.tight_layout()

        coreFig='fig%d_taxon_%s'%(figId,self.name)
        self.save_plot( coreFig )
        self.pltName.append((coreFig,'Average True %s rank vs. choice count'%self.recom))


    #......................
    def plot_fracTop4_2d(self,vx,vy,mxx,figId):
        if len(vx)<2: return
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4))
        ax=self.plt.subplot(1,1, 1)
        myRange=np.array([(0,mxx), (0.5,1.01)])

        hb = ax.hist2d(vx,vy , bins=20, range=myRange, cmin=1, cmap=cmap.cool) #norm=LogNorm())
        ax.set(xlabel='num %s choices per %s'%(self.recom,self.given) ,ylabel='true%s in top4 / knownPairs'%self.recom,title='num spec=%d'%len(vx))
        cb = fig.colorbar(hb[3], ax=ax)
        cb.set_label('spec count')
        ax.grid(color='brown', linestyle='--')
        #ax.set_ylim(0.,10.)
        self.plt.tight_layout()

        coreFig='fig%d_taxon_%s'%(figId,self.name)
        self.save_plot( coreFig )
        self.pltName.append((coreFig,'Average True %s rank vs. choice count'%self.recom))

    #......................
    def save_plot(self, coreFig ):
        figName='%s/%s.%s'%(self.dataPath,coreFig,self.imgForm)
        print('Saving  %s ...'%figName)
        self.plt.savefig(figName)
        self.plt.close()
        os.chmod(figName,0o644) # -rw-r--r-- 


    #----------------
    def makeHTML(self):
        dateStop=datetime.datetime.now()
        dateNowStr=dateStop.strftime("%Y-%m-%d_%H.%M")
       
        taxonN=self.name
        outF=taxonN+'.html'
        self.htmlName='taxons/'+outF
        outF=self.dataPath+outF
        SP3='&nbsp; &nbsp; &nbsp;'
        print('write',outF)
        with open(outF,'w') as fd:
            fd.write("<html>\n<head></head>\n<body>\n")
            fd.write('\n<p>given-%s  generated: %s  Taxonomy Class=<b>%s</b>  Num Species=%d'%(self.given+SP3,dateNowStr+SP3,taxonN+SP3,len(self.specD)))

            self.addAllPlots2HTML(fd)            
            self.printSpeciesTable(fd)

            fd.write("</body> \n</html>\n")
            fd.close()

        os.chmod(outF,0o644) # rw r r 
        
        
   #............................
    def printSpeciesTable(self,fd):

        k=0
        fd.write('\n <p>  <table border="0" > <col width="30"><col width="300">  \n')
        recL=self.tableColumnNames
        
        colL=len(recL)*['bgcolor="yellow"']
        for x in [0,1,2,3]:  colL[x]=''
        
        fd.write('\n <tr> <th> idx  ')        
        for x,col in zip(recL,colL):
            fd.write(' <th  width="80" %s > %s'%(col,x))
        
        for specN in sorted(self.specD):
            k+=1
            fd.write('\n<tr  align="center"><td> %d <td>      <a href="../%s/index.html "> %s </a>  '%(k,specN,specN))

            #print('ss',specN)
            
            #print('ss2',spec)
            #print('ss3',colL)
            for x,col in zip(self.tableColumnNames[1:],colL[1:]):
                val=self.specD[specN][x]
                #print(x,val,col)
                fd.write(' <td  %s> %s '%(col,val))                  


    #---------------------
    def addAllPlots2HTML(self,fd):

        fd.write('\n <hr>  <table border="0" >  <tr>\n')
        k=0
        for name,caption in sorted(self.pltName):
            k+=1
            print('ppp',name)
            txt7all='\n  <td  align="center"> <img src="%s.%s" /> <br>Fig.%d  %s<hr>  '%( name,self.imgForm,k,caption)
            fd.write(txt7all)
        fd.write('\n  </table > \n')

 

