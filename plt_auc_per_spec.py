#!/usr/bin/env python
""" 
must be run after csv per species were produced by score_Kinase.py  
Input: /givenRR/data/
     specName1.givenRR.csv, specName2.givenRR.csv, ...

"""
import numpy as np
import os, time
import shutil
import datetime
import math
import csv

from matplotlib.colors import LogNorm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='summary of AUC & Acc from kfold method for Kinase Oracle',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--inpPath",
                        default='/global/cscratch1/sd/balewski/kinase10b10-plots/givenRR/data',help="path to input")

    parser.add_argument("--plotPath",
                        default='plot',help="path to out plots")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class AUC_per_spec(object):
    def __init__(self, args): 
        self.specD={}
        self.first=True

        allL=os.listdir(args.inpPath)
        csvL=[]
        for x in allL:
            if 'csv' not in x: continue
            print(x)
            csvL.append(x)
            #if len(csvL) >100 : break
        print('found %d species'%len(csvL))
        assert len(csvL) >0

        for x in sorted(csvL):
            self.harvest_species(args.inpPath+'/'+x)
            #break #tmp
            
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-serverx2
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')

        self.plt=plt
        self.figL=[]
        self.pltName=[]

    #............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/auc_per_spec_%s_f%d'%(args.plotPath,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()


    #............................
    def harvest_species(self,fname):
        #print('harvest species from',fname)
        with open(fname) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            k=0;
            for row in readCSV:                
                if k==0 and self.first:
                    self.first=False
                    self.tableColumnNames=[ x.strip() for x in row[:-1] ]
                    print('tab col', self.tableColumnNames)
                if k==1:
                    valL=[ x.strip() for x in row ]
                    #print('tab val',valL)
                k+=1
        
        specN=valL[1]
        assert specN not in self.specD
        self.specD[specN]=valL


    #............................
    def draw_auc(self):

        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(4,4))
        nrow,ncol=1,1
        
        v1=[]; 
        for specN in self.specD:
            rec=self.specD[specN]
            x=rec[10]
            if 'n/a' ==x: continue
            v1.append(float(x))
            #if  rec['nPos'] >100: print(rec,specN)
        
        xx=np.array(v1)
        avr_auc=xx.mean()
        std_auc=xx.std()
        avr_str="%.3f +/- %.3f"%(avr_auc,std_auc)
       

        ax=self.plt.subplot(nrow,ncol,1)
        n, bins, patches=ax.hist(v1, 40, facecolor='g', alpha=0.75)
        ax.set(xlabel='AUC of ROC',ylabel='species count',title='all species')
        ax.set_yscale('log')
        ax.set_xlim(0.9,)
        ax.text(0.91,12.,'avr AUC:'+avr_str)
        ax.grid(color='brown', linestyle='--',which='both')


    #............................
    def draw_nHK_nRR(self):

        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(4,4))
        nrow,ncol=1,1
        
        vHK=[]; vRR=[] 
        for specN in self.specD:
            rec=self.specD[specN]
            nhk=float(rec[2])
            nrr=float(rec[3])
            vHK.append(nhk)
            vRR.append(nrr)
            #if  rec['nPos'] >100: print(rec,specN)
            if nrr>150 or nhk>110 : print(rec,specN)

        '''
        xx=np.array(v1)
        avr_auc=xx.mean()
        std_auc=xx.std()
        avr_str="%.3f +/- %.3f"%(avr_auc,std_auc)
        '''

        ax=self.plt.subplot(nrow,ncol,1)
        n, bins, patches=ax.hist(vRR, 40, facecolor='g', alpha=0.65,label='RR')
        n, bins, patches=ax.hist(vHK, 40, facecolor='r', alpha=0.65,label='HK')
        ax.set(xlabel='num proteins',ylabel='species count',title='all species')
        ax.set_yscale('log')
        ax.legend(loc='best')

        #ax.set_xlim(0.9,)
        #ax.text(0.91,12.,'avr AUC:'+avr_str)
        ax.grid(color='brown', linestyle='--',which='both')


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=AUC_per_spec(args)

ppp.draw_auc()
ppp.draw_nHK_nRR()
ppp.pause(args,'auc-final')
