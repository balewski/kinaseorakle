from sklearn.metrics import roc_curve, auc
import numpy as np
from matplotlib.colors import LogNorm
from matplotlib import cm as cmap
import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model
import yaml
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Kinase(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args): 
        self.venue=args.venue 
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        self.plt=plt
        self.nr_nc=(3,3)
        self.figL=[]

#............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(args.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

#............................
    def plot_paper_inputs(self,deep,figId1, figId2):
        
        v1=[];  v2=[] ; v3=[]
        xV={'HK':[],'RR':[]}
        for specN in deep.coll:
            spec=deep.coll[specN]
            v1.append(len(spec['HK']))
            v2.append(len(spec['RR']))
            nGP=spec['goodPair']
            if nGP>0:
                v3.append(nGP)
            for role in deep.mr12:
                for protN in spec[role]:
                    if 'pair' not in spec[role][protN]: continue
                    xV[role].append(len( spec[role][protN]['seq']))
        #for role in deep.mr12:
        #    print(role,'got xV len',len(xV[role]))

        avr_hk=np.array(v1).mean()        
        avr_rr=np.array(v2).mean()        
        avr_gp=np.array(v3).mean()

        bins = np.linspace(0.0, 150., 50)

        self.figL.append(figId1)
        fig=self.plt.figure(figId1,facecolor='white', figsize=(4.3,3.2))
        ax=self.plt.subplot(1,1,1)
        
        txt=''
        if self.venue=='web': txt=', avr=%.0f'%avr_rr
        ax.hist(v2, bins, histtype='step',color='red',lw=2, alpha=0.75, label='RR proteins'+txt)

        if self.venue=='web': txt=', avr=%.0f'%avr_hk
        ax.hist(v1, bins, alpha=0.75, label='HK proteins'+txt,histtype='step',color='black',lw=2,linestyle='dashed')

        if self.venue=='web': txt=', avr=%.0f'%avr_gp
        ax.hist(v3, bins, facecolor='b', alpha=0.5, label='cognate pairs'+txt)

        ax.set(xlabel='size per species',ylabel='species count')
        ax.set_yscale('log')
        ax.legend(loc='upper right', title='%d species'%len(v3))

        self.figL.append(figId2)
        fig=self.plt.figure(figId2,facecolor='white', figsize=(4.3,2.5))
        ax=self.plt.subplot(1,1,1)

        myRange=np.array([(0,150), (0,120)])
        hb = ax.hist2d(xV['RR'],xV['HK'] , bins=20, range=myRange, cmap=cmap.cool, norm=LogNorm())#, cmin=1
        ax.set(xlabel='RR protein length' ,ylabel='HK protein length')
        cb = fig.colorbar(hb[3], ax=ax)
        cb.set_label('cognate pairs count')
        if self.venue=='web':
            ax.grid() #color='brown', linestyle='--')
            ax.text(10,10,'total %d good pairs'%len(xV['HK']))
        ax.axhline(y=deep.seqLenCut['A'],linewidth=2, color='red', linestyle='--')
        ax.axvline(x=deep.seqLenCut['B'],linewidth=2, color='red', linestyle='--')


#............................
    def plot_model(self,deep):
        #print('plot_model broken in TF=1.4, skip it')
        #return
        if 'cori' not in socket.gethostname(): return  # software not installed
        fname=deep.outPath+'/'+deep.name+'_graph.svg'
        plot_model(deep.model, to_file=fname, show_layer_names=True)#, show_shapes=True)
        print('Graph saved as ',fname)


#............................
    def plot_train_history(self,deep,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(13,6))
        nrow,ncol=self.nr_nc
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (1,0), colspan=2 )

        DL=deep.train_hirD
        outF=args.outPath+'/train_history.yml'
        ymlf = open(outF, 'w')
        yaml.dump(DL, ymlf)
        ymlf.close()
        print('  closed  yaml:',outF)

        balaT,ndataT=DL['info']['train']
        balaV,ndataV=DL['info']['valid']

        lossVe=DL['val_loss'][-1]
        lossTe=DL['loss'][-1]

        tit1='%s, batch=%d, train=%.2f h, end-loss: val=%.3f, train=%.3f'%(deep.name,DL['info']['batch'],deep.train_sec/3600.,lossVe,lossTe)
        ax1.set(ylabel='loss',title=tit1)
        ax1.plot(DL['loss'],'.-.',label='train_%s n=%d'%(balaT,ndataT))
        ax1.plot(DL['val_loss'],'.-',label='valid_%s n=%d'%(balaV,ndataV))
        ax1.legend(loc='best')
        ax1.grid(color='brown', linestyle='--',which='both')

        accVe=DL['val_acc'][-1]
        accTe=DL['acc'][-1]
        tit2='arrIdx=%d, end-acc: val=%.3f, train=%.3f'%(args.arrIdx,accVe,accTe)
        ax2 = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=2,sharex=ax1)
        ax2.set(xlabel='epochs',ylabel='accuracy',title=tit2)
        ax2.plot(DL['acc'],'.-.',label='train_%s'%balaT)
        ax2.plot(DL['val_acc'],'.-',label='valid_%s'%balaV)
        ax2.legend(loc='lower right')
        ax2.grid(color='brown', linestyle='--',which='both')

        if 'lr' not in DL: return

        ax3 = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2,sharex=ax1 )
        ax3.plot(DL['lr'],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')



#............................
    def plot_ROC(self,name,deep,args,figId=10,maxEve=12000):         
        topK=4
        fpr_cut=topK/80.
        nrow,ncol=self.nr_nc

        if figId==10 : 
            self.plt.figure(figId)
        else:
            inXY=12,16
            if  args.venue!='web': inXY=3.,3.
            self.plt.figure(figId,facecolor='white', figsize=(inXY))
            self.figL.append(figId)

            
        XA=deep.data['XA_'+name] 
        XB=deep.data['XB_'+name] 
        XC=deep.data['XC_'+name] 
        y_true=deep.data['Y_'+name]

        print('Produce AUC of ROC, name=',name,'Y shape',y_true.shape)
        m=len(y_true)
        if m>maxEve:
            k=int(m/maxEve)
            print('reduce size from %d  by factor of %d'%(m,k))
            XA=XA[::k]
            XB=XB[::k]
            XC=XC[::k]
            y_true=y_true[::k]

        nTrue=sum(y_true)
        trueFract=nTrue/len(y_true)
        y_pred = deep.model.predict([XA,XB,XC]).flatten()
        print('  nTrue=%d  trueFrac=%.2e'%(nTrue,trueFract))
        self.qa={name:zip(y_true, y_pred)}

        fpr, tpr, _ = roc_curve(y_true, y_pred)
        roc_auc = auc(fpr, tpr)

        outF=args.outPath+'/'+name+'_ROC.csv'
        print('  AUC: %f' % roc_auc,', save:',outF)
        
        with open(outF,'w') as file:
            file.write('# FPR, TPR \n')
            for x,y in zip(fpr,tpr):
                file.write('%.5f,%.5f\n'%(x,y))
            file.close()
 
        plr=np.divide(tpr,fpr)
        for x,y,z in zip(fpr,tpr,plr):            
            if x <fpr_cut :continue
            print('  found fpr=',x, ', tpr=',y,', LR+=',z)
            break

        if args.venue=='web':
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax1 = self.plt.subplot2grid((nrow,ncol), (1,2), rowspan=2)
        else:
            ax1=self.plt.subplot(1,1,1)
        txt=''
        if args.venue=='web': txt=',n=%d'%len(y_pred)
        ax1.plot(fpr, tpr, label='ROC'+txt,color='seagreen' )
        ax1.plot([0, 1], [0, 1], 'k--', label='coin flip')
        ax1.axvline(x=x,linewidth=1, color='blue',linestyle='dotted')
        ax1.set(xlabel='false positive rate',ylabel='true positive rate')
        ax1.plot([0], [1], linestyle='None', marker='+', color='magenta', markersize=15,markeredgewidth=2, label='ideal classifier')

        ax1.set_ylim(0.4,1.05) ;   ax1.set_xlim(-0.05,0.6) 
        if args.venue=='web':
            ax1.grid(color='brown', linestyle='--',which='both')
            ax1.legend(loc='center right', title=name)
            ax1.set_title('ROC , area = %.4f' % roc_auc)
            ax1.text(x*1.1,y*0.9,"%.4f @ %.4f"%(y,x),color='blue')
        else:
            ax1.legend(loc='center right')
            return

        ax2 = self.plt.subplot(nrow,ncol, 3)
        ax2.plot(fpr,plr, label='ROC', color='teal')
        ax2.plot([0, 1], [1, 1], 'k--',label='coin flip')
        ax2.set(ylabel='Pos. Likelih. Ratio',xlabel='False Positive Rate',title='LR+(%.3f)=%.2f, %s'%(x,z,name))
        ax2.set_xlim([0.,fpr_cut+0.05])
        ax2.set_ylim([0.,2*z])

        ax2.axvline(x=x,linewidth=1, color='blue')
        ax2.legend(loc='lower middle')
        ax2.grid(color='brown', linestyle='--',which='both')


#............................
    def plot_labeledScores(self,name,deep,args,figId):         
        assert figId!=10 
        assert name in self.qa

        if figId not in self.figL:
            self.plt.figure(figId,facecolor='white', figsize=(4.3,2.5))
            self.figL.append(figId)
            ax=self.plt.subplot(1,1,1)
        else:
            self.plt.figure(figId)
            nrow,ncol=self.nr_nc
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot2grid((nrow,ncol), (1,0), rowspan=2, colspan=2)
        

        # split scores according to the truth 
        score_thr=0.50
        u={0:[],1:[]}
        mAcc={0:0,1:0,'u':0}
        for ygt,ysc in self.qa[name]:
            u[ygt].append(ysc)
            if ysc> score_thr : mAcc[ygt]+=1

        mInp={0:len(u[0]),1:len(u[1])}

        print('Labeled scores found mAcc',mAcc, ' thr=',score_thr)

        bins = np.linspace(0.0, 1., 50)
        txt=''
        if self.venue=='web': txt=',TPR=%.2f'%(mAcc[1]/mInp[1])
        ax.hist(u[1], bins, alpha=0.6,label='%d cognate pairs out of %d'%(mAcc[1],mInp[1])+txt)
        if self.venue=='web': txt=', FPR=%.2f'%(mAcc[0]/mInp[0])
        ax.hist(u[0], bins, alpha=0.5,label='%d not-cognate pairs out of %d'%(mAcc[0],mInp[0])+txt)

        ax.axvline(x=score_thr,linewidth=2, color='blue', linestyle='--')

        ax.set(xlabel='predicted score', ylabel='num samples')
        ax.set_yscale('log')
        if self.venue=='web': 
            ax.grid(True)
            ax.set_title('Labeled scores for segment:  %s'%(name))
        ax.legend(loc='upper right', title='score thr > %.2f'%score_thr)


        ax.set_xlim([-0.02,1.02])

        return

