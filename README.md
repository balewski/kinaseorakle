# README #

### What is this repository for? ###

* recommend topK HK-RR protein matches, ver 7.1
* Input data taken from P2CS:  http://www.p2cs.org/ using p2cs_dumpTaxonomy.py 

### How do I get set up? ###

* Requires Python3, TensorFlow, Keras
* Data pre-processing: format_Kinase.py
* fitting/re-train: train_Kinase.py
* predicting: predict_Kinase.py
* Generating recommendations:  recommend_Kinase.py, publish_Kinase.py
* one-off analysis of predictions (w/o TF): explore_one.py
* kfold-traing (K=10) : bigLoop_kfoldOne.sh + kfold_one_eval.py
* kfold-analysis, merging: plot_kfold_avr.py,  ana_kfold_geobacter.py

### Who do I talk to? ###

* Jan Balewski : janstar1122@gmail.com
* Zach Hallberg: zhallber@berkeley.edu
