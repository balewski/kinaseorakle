import numpy as np
import datetime
import os, time
import yaml
import shutil

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from PubTaxon_Kinase import PubTaxon_Kinase

#............................
#............................
#............................
class PubAll_Kinase(object):

    def __init__(self, args, maxSpec=9999): 
        self.deepName=args.deepName
        self.verb=args.verb
        self.given=args.given  
        self.recom='HK'
        if self.given=='HK':self.recom='RR'
      
        self.pubTaxon={} # [taxonN]

        self.dataPath=args.dataPath+'/given'+args.given
        self.webPath=args.webPath+'/given'+args.given
        
        print('dataPath',self.dataPath)
        allL=os.listdir(self.dataPath)
        k=0
        start = time.time()
        for x in allL:
            #if '~' in x: continue
            specN=x
            sumaN=self.dataPath+'/'+x+'/summary.yml'
            #print('sumaN',sumaN)
            if k >= maxSpec : break
            if not os.path.exists(sumaN): continue
            with open(sumaN) as ymlf:
                bulk=yaml.load( ymlf)
                taxonN=bulk['Taxonomy class']
                #taxonN='any123'
                k+=1
                if taxonN not in self.pubTaxon : self.pubTaxon[taxonN]=PubTaxon_Kinase(args,taxonN,self.dataPath)
                self.pubTaxon[taxonN].add_species(bulk)
    
        print('found %d species  given %s'%(k,self.given))
        assert k >0
        #print(self.specD.keys())
        print('species loop end  elaT=%.1f sec , found %d taxon classes'%(time.time() - start,len(self.pubTaxon)))


    #----------------
    def processTaxons(self):
        print('generate taxon HTML pages')
        for taxon in self.pubTaxon:
            self.pubTaxon[taxon].process()
            #break

    #----------------
    def coverHTML(self):
        dateStop=datetime.datetime.now()
        dateNowStr=dateStop.strftime("%Y-%m-%d_%H.%M")
        
        outF=self.dataPath+'/index.html'
        SP3='&nbsp; &nbsp; &nbsp;'
        
        with open(outF,'w') as fd:
            fd.write("<html>\n<head></head>\n<body>\n")
            fd.write('\n<p>%s  given-%s  generated: %s'%(self.deepName+SP3,self.given+SP3,dateNowStr))

            fd.write('\n <p>  <table border="0" >  <tr> <th> Taxonomy class <td> %s num species <td> %s avr pred. %s rank\n'%(SP3,SP3,self.recom))
            for taxon in sorted(self.pubTaxon):
                numSpec=self.pubTaxon[taxon].numSpec()
                taxonHtml=self.pubTaxon[taxon].htmlName
                avrStr='n/a'
                if numSpec>2:
                    avrStr=self.pubTaxon[taxon].avrStr
                
                fd.write('\n <tr> <th><a href="%s" > %s </a><td>%s %d <td>%s'%(taxonHtml,taxon,SP3,numSpec,SP3+avrStr))

            fd.write('\n  </table  >')

            fd.write("</body> \n</html>\n")
            fd.close()

        os.chmod(outF,0o664) # rw r r 
        # cover page is done



 


