# small operations w/o dependencies
# keep double indent - in case you want to make a class of it

import os, time
import shutil

    #............................
def getGeneAddress(protN):
        xL=protN.split('_')
        #print(protN,len(xL),xL)
        #find longest string of numbers, use the last one if more found 
        mxDig=0; mxVal=-1
        for x in xL:
            if len(x) <2 : continue
            if not x[-1].isdigit():  x=x[:-1] # allow for 1 character to be skipped
            if not x[-1].isdigit(): continue
            #print('Q',x)
            val=0
            dig=1
            for y in reversed(list(x)):
                #print(y)
                if not y.isdigit(): break
                val+=dig*int(y)
                dig*=10
            #print('end', dig,val)
            if mxDig>dig: continue  # will use the later number if two are present
            mxDig=dig
            mxVal=val
        if mxVal<=0: print(protN,len(xL),xL)
        assert mxVal>0
        return mxVal


    #............................
def read_proteins_fasta(fName,aminoacidSet):
        protDBL=[]     
        print('read from ',fName)   
        fp = open(fName, 'r')
        protN=''
        for line in fp:
            line = line.rstrip()
            if line[0]=='>':#  header: Name|Species|association
                #print(lineL)
                if len(protN) >0: #archive last protein
                    protDBL.append( [protN,specN,assoc,seq])
                    protN='bad1'; seq='bad2'

                lineL=line[1:].split('|')
                protN = lineL[0]
                specN=lineL[1]
                idx4=specN.rfind('plasmid')  # drop specN extension=plasmid
                if idx4>0: specN=specN[:idx4]            
                specN=specN.strip()
                assoc=lineL[2].strip()
                seq=''
            else:    # - - - - it is the sequence
                assert aminoacidSet.issuperset(set(line))
                assert len(protN)>0
                seq+=  line
        fp.close()
        
        print('  read proteins inp list len=',len(protDBL))
        return protDBL



    #............................
def createPubDir(path0,child):
        print(' createPubDir path0=',path0)
        assert(os.path.isdir(path0))
        assert(len(child)>1)
        path1=path0+'/'+child
        #print('create dir=',path1)
        if os.path.isdir(path1):
                path2=path1+'_Old'
                if os.path.isdir(path2):
                        shutil.rmtree(path2)
                os.rename(path1,path2)
        try:
            os.makedirs(path1)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

        os.chmod(path1,0o765) # a+r a+x
        print(" created dir "+path1)
        return path1




# special species:

excludeL_zach=[
    'Acidimicrobium ferrooxidans DSM 10331', #: {HK: 9, RR: 15, goodPair: 7, taxon: Other}
    'Geobacter sulfurreducens PCA', #: {HK: 89, RR: 115, goodPair: 33, taxon: Deltaproteobacteria}
    'Vibrio cholerae M66-2', #: {HK: 34, RR: 64, goodPair: 22, taxon: Gammaproteobacteria}
    'Escherichia coli 536', #: {HK: 26, RR: 35, goodPair: 18, taxon: Gammaproteobacteria}
    'Myxococcus xanthus DK 1622', #: {HK: 98, RR: 161, goodPair: 24, taxon: Deltaproteobacteria}
    'Caulobacter crescentus CB15' #: {HK: 50, RR: 71, goodPair: 11, taxon: Alphaproteobacteria}
]

hardL=[  
   'Anaerolinea thermophila UNI-1', #: {goodPair: 23, nHK: 37, nRR: 58}
   'Arcobacter nitrofigilis DSM 7299', #: {goodPair: 33, nHK: 57, nRR: 74}
   'Clostridium saccharolyticum WM1', #: {goodPair: 41, nHK: 44, nRR: 62}	
   'Paenibacillus sp. Y412MC10' #: {goodPair: 108, nHK: 130, nRR: 138}
] # hard cases

no_pairsL=['Staphylococcus aureus subsp. aureus M013'] # no pairs

excludeL_bigGluts=[
        'Azospirillum brasilense Sp245',
        'Azospirillum lipoferum 4B',
        'Azospirillum sp. B510',
        'Chroococcidiopsis thermalis PCC 7203',
        'Desulfatibacillum alkenivorans AK-01',
        'Singulisphaera acidiphila DSM 18658'
]


excludeL_all=excludeL_zach+excludeL_bigGluts
