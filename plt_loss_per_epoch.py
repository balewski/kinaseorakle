#!/usr/bin/env python
""" 
Reads training output , does various post-training plots
"""
import numpy as np
import os, time
import shutil
import datetime
import yaml
import math

from matplotlib.colors import LogNorm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='summary of AUC & Acc from kfold method for Kinase Oracle',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--inpPath",
       default='/global/cscratch1/sd/balewski/kinase10_sum/10e/',
       #default='/global/cscratch1/sd/balewski/',
                        help="path to input")

    parser.add_argument("--plotPath",
                        default='plot',help="path to out plots")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument('--venue', dest='venue', choices=['web','icml'],
                         default='web',
                         help="change fromat of output plots to match publisher requirements")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Loss_per_epoch(object):
    def __init__(self, args,name):
        self.name=name
        self.venue=args.venue

        inpF=args.inpPath+'%s/train_history.yml'%name
        print('input=',inpF)
        
        ymlf = open(inpF, 'r')
        bulk=yaml.load( ymlf)
        ymlf.close()
        for x in bulk:
            print ('  %s:%d,'%(x, len(bulk[x])),end='')
        print()
        assert len(bulk['lr'])>2

        self.DL=bulk
                    
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-serverx2
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')

        self.plt=plt
        self.figL=[]
        self.pltName=[]

    #............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/auc_per_spec_%s_f%d'%(args.plotPath,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

    #............................
    def draw_score_2D(self,yn):
        ynN={0:['negative','False Positive Rate'],1:['positive','False Negative Rate']}
        labN,ratN=ynN[yn]
        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,6))
        nrow,ncol=2,2
        ax2=self.plt.subplot(nrow,ncol,3)
        ax3=self.plt.subplot(nrow,ncol,4)
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2 )

        balaN,ndataV=self.DL['info']['valid']
        segN='valid_'+balaN

        inpL=self.DL['confusion']
        nE=len(inpL)
        nB=len(inpL[0][0]) # epoch=0, yn=0
        nB2=int(nB/2)
        print('yn=%s see %d epochs , nBins=%d'%(labN,nE,nB))

        score2D=[]
        ratL=[]
        for epo in inpL:
            recL=inpL[epo][yn]
            sum1=sum(recL)
            sum2=sum(recL[nB2:])
            del12=sum1-sum2
            if yn==0:
                x=sum2/sum1
            else:
                x=del12/sum1
            #print(epo,recL,sum1,sum2,del12,x)
            ratL.append(x)
            score2D.append([i for i in reversed(recL)])

        score3=np.array(score2D).T
        extent = [-0.5,nE-0.5,0,1]

        im=ax1.imshow(score3, extent=extent,cmap=self.plt.cm.rainbow, norm=LogNorm(), aspect = 'auto')
        self.plt.colorbar(im).set_label('pair count')
        ax1.axhline(y=0.5,linewidth=2, color='brown', linestyle='--')
        ax1.set(xlabel='epochs, '+self.name,ylabel='score', title='%s, truth=%s'%(segN,labN))
        ax1.text(nE/3,0.7,'Accepted',rotation=10)


        ax2.plot(ratL,'.-',label=segN+', '+labN)
        ax2.set(xlabel='epochs', title=ratN)
        ax2.grid(color='brown', linestyle='--',which='both')
        ax2.set_ylim(0.,)
        ax2.text(1,0.01,segN)


        sX=np.linspace(0,1,nB)
        dE=3
        jj=int(nE/dE)
        eL=[j*dE for j in range(jj)]
        if eL[-1] <nE-1 : eL.append(nE-1)
        for epo in eL:
            ax3.plot(sX,inpL[epo][yn],'_-',label='%d'%epo)

        ax3.set_yscale('log')
        ax3.legend(loc='upper center',title='epoch')
        ax3.set(xlabel='score', title='pairs')
        ax3.grid(color='brown', linestyle='--',which='both')

    #............................
    def draw_loss_acc_1D(self):

        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(4.3,5))
        nrow,ncol=2,1
        ax1=self.plt.subplot(nrow,ncol,1)
        ax2=self.plt.subplot(nrow,ncol,2)
   
        DL=self.DL
        balaT,ndataT=DL['info']['train']
        balaV,ndataV=DL['info']['valid']

        loss1=DL['val_loss'][-1]

        if self.venue=='web':
            ax1.set_title('%s, end-val-loss=%.3f'%(self.name,loss1) )
            lblT='train_%s n=%d'%(balaT,ndataT)
            lblV='valid_%s n=%d'%(balaV,ndataV)
            ax1.grid()#color='brown', linestyle='--',which='both')

        else:
            lblT='train'
            lblV='valid'
                                
                                  
        ax1.set(ylabel='loss')

        ax1.plot(DL['loss'],'.-',label=lblT)
        ax1.plot(DL['val_loss'],'.-',label=lblV)
        ax1.legend(loc='best')

        acc=DL['val_acc'][-1]
        if self.venue=='web':
            ax2.set_title(' end-val-acc=%.4f'%(acc))
            ax2.grid()#color='brown', linestyle='--',which='both')

        ax2.set(xlabel='epochs',ylabel='accuracy')
        ax2.plot(DL['acc'],'.-',label='train')
        ax2.plot(DL['val_acc'],'.-',label='valid')
        ax2.legend(loc='lower right')

  

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

name='kinase10e-12'

ppp=Loss_per_epoch(args,name)

ppp.draw_loss_acc_1D()
ppp.draw_score_2D(0) # negative
ppp.draw_score_2D(1) # positive
ppp.pause(args,name)
