#!/usr/bin/env python
""" 
must be run after kfold yaml were produced by bigLoop_kfoldOne.sh
agregate kfold per species 
read yaml kfold_one summaries,
"""
import numpy as np
import os, time
import shutil
import datetime
import yaml
import math

from matplotlib.colors import LogNorm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='summary of AUC & Acc from kfold method for Kinase Oracle',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--inpPath",
                        default='outKF',help="path to input")

    parser.add_argument("--plotPath",
                        default='plotKF',help="path to out plots")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Kfold_AUC_per_spec(object):
    def __init__(self, args,segN): 
        self.specD={}

        allL=os.listdir(args.inpPath)
        yamlL=[]
        for x in allL:
            if 'yml' not in x: continue
            if 'kfold' not in x: continue
            if 'idx2'  in x: continue # use only 1st 10 kfolds
            yamlL.append(x)
        #print('found %d yaml-kfolds'%len(yamlL))
        assert len(yamlL) >0

        for x in sorted(yamlL):
            self.harvest_species(args.inpPath+'/'+x,segN)
            #break #tmp
            
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-serverx2
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')

        self.plt=plt
        self.figL=[]
        self.pltName=[]

    #............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/auc_per_spec_%s_f%d'%(args.plotPath,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()


    #............................
    def harvest_species(self,inpF,segN):
        print('harvest species from',inpF,' seg=',segN)
        ymlf = open(inpF, 'r')
        inpD=yaml.load( ymlf)[segN]['specD']
        ymlf.close()
        print('  found %d records'%len(inpD))
        assert len(inpD) >0
        for x in inpD:
            #print(x)
            assert x not in self.specD
            self.specD[x]=inpD[x]
        print('  total %d species'%len( self.specD))


    #............................
    def pass1_plots(self):

        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(11,6))
        nrow,ncol=2,2
        
        v1=[];  v2=[] ; v3=[];v4=[]
        for specN in self.specD:
            rec=self.specD[specN]
            v1.append(rec['nPos'])
            v4.append(math.log10(rec['nPos']))
            v2.append(rec['nNeg']/1000.)
            v3.append(rec['auc'])
            if rec['auc'] <0.8 and rec['nPos'] >30: print(rec,specN)
            if  rec['nPos'] >100: print(rec,specN)
        
        ax=self.plt.subplot(nrow,ncol,2)
        n, bins, patches=ax.hist(v1, 50, facecolor='b', alpha=0.75)
        ax.set(xlabel='positive pairs',ylabel='spec count')
        ax.set_yscale('log')

        ax=self.plt.subplot(nrow,ncol,1)
        n, bins, patches=ax.hist(v2, 50, facecolor='salmon', alpha=0.75)
        ax.set(xlabel='negative pairs/1000',ylabel='spec count')
        ax.set_yscale('log')

        ax=self.plt.subplot(nrow,ncol,3)
        n, bins, patches=ax.hist(v3, 50, facecolor='g', alpha=0.75)
        ax.set(xlabel='AUC of ROC',ylabel='spec count')
        ax.set_yscale('log')

        ax=self.plt.subplot(nrow,ncol,4)
        hb = ax.hist2d(v1,v3 , bins=20, norm=LogNorm())
        ax.set(xlabel='positive pairs' ,ylabel='AUC of ROC',title='num spec=%d'%len(v1))

        #hb = ax.hist2d(v4,v3 , bins=20, norm=LogNorm())
        #ax.set(xlabel='log10(positive pairs)' ,ylabel='AUC of ROC',title='num spec=%d'%len(v1))
        cb = fig.colorbar(hb[3], ax=ax)
        cb.set_label('spec count')
        ax.grid(color='brown', linestyle='--')
  

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

segN='val'
segN='test_natur'
ppp=Kfold_AUC_per_spec(args,segN)

ppp.pass1_plots()
ppp.pause(args,segN)
