#!/usr/bin/env python
"""  read raw input data
sanitize, split, ballance correction
write data as tensors: (train,val,test)
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#exit(2) # block  overwrite

from Deep_Kinase import Deep_Kinase
from Plotter_Kinase import Plotter_Kinase


import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Format P2CS input data for Kinase Oracle training',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots")
    parser.add_argument( "-X","--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument('--venue', dest='venue', choices=['web','icml'],
                         default='web',
                         help="change fromat of output plots to match publisher requirements")

    args = parser.parse_args()
    args.prjVersion='10'
    args.prjName='kinase'+args.prjVersion # core name used everywhere

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    args.arrIdx=0 # for plotter, not needed here
    args.kfoldOffset=0 # for training, not needed here
    args.tf_timeline=False # only for training
    args.noiseFract=0  # only for training
    return args

from Util_Kinase import excludeL_all as excludeL_all
#print('BBB',excludeL_all)

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_Kinase(args )
deep=Deep_Kinase(args)

deep.load_taxonomy()
deep.load_proteins_fasta('HK')
deep.load_proteins_fasta('RR')
deep.print_input_raw(3)

specL=sorted(deep.taxonDB.keys()) # all avaliable input
#specL=specL[:100] # for testing

print("Formatter input %d species"%(len(specL)))

sumGoodPairs=0; sumNotPairs=0; nSpecTrain=0
for specN in specL:
    nPair,nHK,nRR=deep.expand_species(specN)      
    sumGoodPairs+=nPair
    sumNotPairs+=nPair*nPair
    if nPair>0: nSpecTrain+=1
    #if sumGoodPairs>50: break
    if nHK>140 or nRR>170 :print('discard: ',specN)
print('M: left with expanded: %d species,%d nSpecTrain, %d good pairs, %d not-pairs'%(len(deep.coll),nSpecTrain,sumGoodPairs,sumNotPairs))

ppp.plot_paper_inputs(deep,8,9)  
ppp.pause(args,'format') # just for graphic display

# pick one below:
deep.split_species(excludeL=excludeL_all)  # is NOT generating pairs

#deep.collect_species_info() ; exit(2) # only saved as YAML 

''' produce:
-oversampling of positive pairs
- natural mixture
-undersampling negative pairs

Nameing scheme:  kinase7.seg2-over-pairs.yml
          dict:  XB_5_under 
  
'''
for bal in ['over','natur']: # ,'under'
    deep.assembly_labeled_pairs(balance=bal)  # skip orpahns and triples, etc.  
    #break




