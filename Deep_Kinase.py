import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
from Oracle_Kinase import Oracle_Kinase

from tensorflow.contrib.keras.api.keras.callbacks import EarlyStopping, ModelCheckpoint ,ReduceLROnPlateau, Callback 
from tensorflow.contrib.keras.api.keras.layers import Dense, Dropout,  LSTM, Input, concatenate
from tensorflow.contrib.keras.api.keras.models import Model , load_model
from tensorflow.contrib.keras.api.keras import backend as K
import tensorflow as tf
from sklearn.metrics import roc_curve, auc

import random
import numpy as np
import yaml
try:
     from yaml import CLoader, CDumper
except ImportError:
     from yaml import Loader as CLoader, Dumper  as CDumper

print('deep-libs2 imported, TF.ver=%s, elaT=%.1f sec'%(tf.__version__,(time.time() - start)))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
class MyLearningTracker(Callback):
     def __init__(self):         
          self.hir=[]          
          self.confusion={}
     def on_epoch_end(self, epoch, logs={}):
          optimizer = self.model.optimizer
          lr = K.eval(optimizer.lr)
          #print([x for x, y in self.__dict__.items()])          
          #print([x for x, y in self.model.__dict__.items()])
          #https://keunwoochoi.wordpress.com/2016/07/16/keras-callbacks/
          self.compute_confusion(epoch)
          self.hir.append(lr)
     def compute_confusion(self,epoch):
          Y=self.validation_data[3].flatten()
          nY=len(Y)
          nTrue=sum(Y)
          #print('\n doing confusion for epoch=',epoch, 'nY=%d nTrue=%d'%(nY,nTrue))
          y_score = self.model.predict(self.validation_data[0:3]).flatten()
          #print('y_score:',y_score[:16], type(y_score))
          nBin=20
          y_bin=(0.999*nBin*y_score).astype(int) # 0.99 is to prevent overflow
          #print('y_bin:',y_bin[:16])          
          scoreH={0:[0]*nBin,1:[0]*nBin}
          for i,ygt in zip(y_bin,Y):
               try:
                    scoreH[ygt][i]+=1
               except:
                    print('janErr1:',i,ygt,y_score[:16],y_bin[:16],Y[:16])
          #print('\nconfusion epoch=',epoch,' scoreH:',scoreH)
          self.confusion[epoch]=scoreH
          
        
from tensorflow.python.client import timeline
#............................
#............................
#............................
class Deep_Kinase(Oracle_Kinase):

    def __init__(self,args):
        Oracle_Kinase.__init__(self,args)
        self.name=args.prjName
        self.noiseFract=args.noiseFract
        print(self.__class__.__name__,', prj:',self.name)
        self.numSegm=10 # for K-fold method
        self.kfoldOffset=args.kfoldOffset

        # data containers
        self.coll_drop={}  # collection of species excluded from ML

        assert os.path.isdir(self.outPath) # check it now to avoid lost time
        if args.tf_timeline:
             # conflicts:
             assert '1.4'  in tf.__version__
             sess = tf.Session()
             K.set_session(sess)


    #............................
    def addNoise(self,seqStr):
         seqL=list(seqStr)
         m=len(seqL)
         k=int(self.noiseFract*m)
         if k<=0 : k=1
         k=min(int(m/3),k)  # not more than 30% noise
         #print('addNoise fract=%f  padC=%c inpLen=%d, nMask=%d'%(self.noiseFract,self.padChar,m,k))

         idxL=np.random.choice( m,k, replace=False)
         #print(idxL)
         for i in idxL:
              seqL[i]=self.padChar
         seqS2=''.join(seqL)
         #print('new seq=',seqS2) 
         #exit(1)
         return seqS2

    #............................
    def print_input_raw(self,k):
        print(k,'samples of taxonDB for species:')
        for x in random.sample(self.taxonDB.keys(),k):
            print(x, self.taxonDB[x])
            
        seqD=self.protDB['HK']
        print('\n',k,'samples of HK-proteins (name, seq-length, all-data)')
        i=0
        for nn in seqD:
            i+=1
            print(nn,len(seqD[nn]['seq']),seqD[nn])
            if i> k: break
        return    

     
    #............................
    def split_species(self, save=True, excludeL=None):
        # it only creates list of species, do not generate pairs
        print('split_species into %d segments'%self.numSegm)
        self.spec_split={} # only for  build_labeled_pairs(...)
        specL= list(self.coll.keys())

        taxonCnt={}        
        out1={};out2={};out0={}; out15={}
        numSegm=self.numSegm*2 # use Gauss trick to balance segments by num pairs        
        for seg in range(numSegm):  
            out0[seg]=[]
            out1[seg]=0
            out2[seg]=0
            out15[seg]=0

        if excludeL:
            print('force %d+ species to segment=%d'%(len(excludeL),self.numSegm))
            zachD=[]
            for x in excludeL:
                if x not in specL: continue
                zachD.append(x)
                specL.remove(x)
            print('force done, specL=%d'%(len(specL)),' was:', len(self.coll))
            self.spec_split[self.numSegm]=zachD
     
        for specN in specL:
            ix=np.random.randint(numSegm)
            out0[ix].append(specN)
            nPair = self.coll[specN]['goodPair']
            out1[ix]+=nPair
            out15[ix]+=pow(nPair,1.5)  # will be used to merge sedata segments
            out2[ix]+=nPair*nPair
            #taxonN=self.taxonDB[specN]['class'] #truth, full resoluton
            taxonN=self.coll[specN]['taxon'] # reduced number of names to ~13
            
            if taxonN not in taxonCnt : taxonCnt[taxonN]=0
            taxonCnt[taxonN]+=nPair
 
        if self.verb>1:
             print('  achieved double-split for  %d species to segments:'%len(specL),[ (x,len(out0[x]),out1[x],out2[x]) for x in out0 ])

             
        taxL0=sorted(list(taxonCnt.keys())) # by name
        taxL0=sorted(taxonCnt, key=taxonCnt.get, reverse=True) #by value

        print('taxon list len:',len(taxL0))
        sumPair=0; taxonL=[]
        for x in  taxL0:            
             sumPair+=taxonCnt[x]
             if taxonCnt[x] <200: continue
             print(x,taxonCnt[x],', sum=',sumPair)
             taxonL.append(x)
        print(' taxon sumPair=',sumPair)
        print(len(taxonL),sorted(taxonL))

        # sort segments  by size metric
        dd=out15
        segL=[ x  for x in sorted(dd, key=dd.get, reverse=True)]
        if self.verb>1:
             print('sorted segments')
             for x in segL:  print(x,dd[x])

        # merge pairs of segments
        for seg in range(self.numSegm):
            i1=segL[seg]
            i2=segL[numSegm-seg-1]
            sum=dd[i1]+dd[i2]
            sum2=out2[i1]+out2[i2]
            print('seg=',seg,':',i1,i2,' nGoodPairs=',int(sum),' nNotPairs=',sum2)
            self.spec_split[seg]= out0[i1]+out0[i2]


        if save==True: self.save_species_split() 

        
    #............................
    def assembly_labeled_pairs(self,balance):
        
        for seg in  self.spec_split: # remember to run split_species(.) firts
            specL=self.spec_split[seg]
            print('build_labeled_pairs seg=%s  numSpec=%d balance=%s'%(seg,len(specL),balance))

            #meaning:  A=HK, B=RR, C=taxon_class
            X={'A':[],'B':[],'C':[]};Y=[]  # final string-data containers
            S=[] # auxiliary , list of species for test & eval only

            print('  step-1  drop orphan from RRs and from HKs') # changed in ver 10b
            for specN in specL:                
                for role in self.mr12:
                     coll=self.coll[specN][role]
                     protL=list(coll.keys())
                     for protN in protL:
                          if 'pair' in coll[protN] : continue
                          coll.pop(protN)
                     if self.verb>1 :
                          print(role,'kOrph', specN, 'inp=',len(protL), 'end=',len(coll))

            print('  step-2  build 2D map of all possible pairs, tag good-/not-')
            nSpec=0; skipSpec=0; nMatch=0; nNotMatch=0
            for specN in specL:
                collHK=self.coll[specN]['HK']        
                collRR=self.coll[specN]['RR']
                nGoodPair=self.coll[specN]['goodPair']
                taxonN=self.coll[specN]['taxon']
                pair2L={1:[],0:[]}  # matched/not-mached pairs 
                xHK=0; xRR=0; xGoP=0
                for hk in collHK:
                    xHK+=1
                    for rr in collRR:
                        xRR+=1
                        match=0
                        if hk in  collRR[rr]['pair'] :
                             match=1
                             xGoP+=1
                             assert  rr in  collHK[hk]['pair']
                        pair2L[match].append([hk,rr,taxonN,match])

                n1=len(pair2L[1]); n0=len(pair2L[0])
                #print('     asmb2: specN=',specN,' n0,1=',n0,n1,'xHK=',xHK,'xRR=',xRR,'xGop=',xGoP,'nGoodPair=',nGoodPair)
                if n1*n0 ==0:
                    print('     tmp1_skip specN=',specN,' n0,1=',n0,n1)
                    skipSpec+=1
                    continue
                if n1>n0:
                    print('     tmp2_skip specN=',specN,' n0,1=',n0,n1)
                    n1=n0
                    

                nSpec+=1
                nMatch+=n1
                nNotMatch+=n0
            
              # balance sample up/donw/none
                if balance=='under':
                    idxL=np.random.choice(n0, n1, replace=False)
                    pairX=pair2L[0]
                    newX=[ pairX[i] for i in idxL ]
                    pair2L[0]=newX
                elif balance=='over':
                    idxL=np.random.choice(n1, n0, replace=True)
                    pairX=pair2L[1]
                    newX=[ pairX[i] for i in idxL ]
                    pair2L[1]=newX
                elif balance=='natur':
                    a=1 # keep natural mixture of positive and negative pairs
                else:
                    print('fatal error, what balance ?', balance)
                    exit(1)

    
                # merge & shuffle 0,1 pairs 
                pairsL=pair2L[0] +pair2L[1]
                n01=len(pairsL)

                if nSpec%50==0 or nSpec<5:
                    print('     nSpec:',nSpec,', Increment [XA,XB,XC],Y for seg:',seg,'allPairs=',n01,specN,' len 1=%d 0=%d'%(len(pair2L[1]), len(pair2L[0])),', init nNoP=%d nGoP=%d'%(n0,n1),' nHK=%d, nRR=%d '%(len(collHK),len(collRR)))

                idxL=np.random.choice(n01,n01, replace=False)
                for i in idxL: # shuffle pairs within one sepecies
                    hk,rr,tax,match=pairsL[i]
                    X['A'].append(hk)  
                    X['B'].append(rr)  
                    X['C'].append(tax)
                    Y.append(match)
                    S.append(specN)

            balStr="-"+balance
            print('  step-3   save final tensors  balance=',balStr,' len=',len(Y))
            out={}
            for ab in ['A','B','C']:
                out['X'+ab+balStr]=X[ab]
            out['Y'+balStr]=Y
            out['S'+balStr]=S

            self.save_pairs_yaml(out,seg,balance)    
            print('end_of_segment=%d nMatch=%d nNotMatch=%d nSpec=%d skipSpec=%d'%(seg,nMatch,nNotMatch,nSpec,skipSpec))        
        return

    #............................
    def build_compile_model(self,args,ext):
        shA=self.data['XA_train_'+ext].shape
        shB=self.data['XB_train_'+ext].shape
        shC=self.data['XC_train_'+ext].shape
        
        inputA = Input(shape=(shA[1],shA[2]), name='HK_seq')
        inputB = Input(shape=(shB[1],shB[2]), name='RR_seq')
        inputC = Input(shape=(shC[1],), name='taxonomy')
        print('build_model inpA:',inputA.get_shape(),' inpB:',inputB.get_shape(),' inpC:',inputC.get_shape())


        # model 7b
        lstm_A1=65; lstm_B1=110
        lstm_A2=65; lstm_B2=110
        dens_n3=30
        dens_n2=dens_n3*2
        dens_n1=dens_n2*2

        densAct='relu'

        #layerDropFrac=0.1+ (args.arrIdx%5)/10.
        layerDropFrac=args.dropFrac
        recDropFrac=layerDropFrac/2.

        print('Dens act=',densAct,' recurDropFrac=',recDropFrac,' layerDropFrac=',layerDropFrac,' idx=',args.arrIdx)
        
        netA= LSTM(lstm_A1, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='A1_%d'%lstm_A1,return_sequences=True) (inputA)

        netA= LSTM(lstm_A2, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='A2_%d'%lstm_A2) (netA)

        netB= LSTM(lstm_B1, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='B1_%d'%lstm_B1,return_sequences=True) (inputB)
        netB= LSTM(lstm_B2, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='B2_%d'%lstm_B2) (netB)

        net = concatenate([netA,netB,inputC],name='add_ABC')
        net=Dense(dens_n1, activation=densAct, name='D_%d'%dens_n1)(net)
        net=Dropout(layerDropFrac)(net)

        net=Dense(dens_n2, activation=densAct, name='D_%d'%dens_n2)(net)
        net=Dropout(layerDropFrac)(net)

        net=Dense(dens_n3, activation=densAct, name='D_%d'%dens_n3)(net)
        net=Dropout(layerDropFrac)(net)

        outputs=Dense(1, activation='sigmoid', name='decision_1')(net) # predicts only 0/1 
        model = Model(inputs=[inputA,inputB,inputC], outputs=outputs)
        self.model=model

        self.compile_model(doTimeline=args.tf_timeline)


    #............................
    def load_compile_1model(self,path='.'):  # for re-training
        try:
            del self.model
            print('delet old model')
        except:
            a=1
        start = time.time()
        inpF5m=path+'/'+self.name+'.model.h5'
        print('load model and weights  from',inpF5m,'  ... ')
        self.model=load_model(inpF5m) # creates mode from HDF5
        #self.model.summary()
        print(' model loaded, elaT=%.1f sec'%(time.time() - start))
        self.compile_model()


    #............................
    def compile_model(self,doTimeline=False):
        """ https://machinelearningmastery.com/save-load-keras-deep-learning-models/
        It is important to compile the loaded model before it is used. 
        This is so that predictions made using the model can use 
        the appropriate efficient computation from the Keras backend.
        """
        print(' (re)Compile model, timeline=',doTimeline)
 
        start = time.time()
        run_metadata =None 
        if doTimeline:  # include timeline options
             run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
             run_metadata = tf.RunMetadata()
             tf_sess_run_kwargs = {'options': run_options, 'run_metadata': run_metadata}
             self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'], **tf_sess_run_kwargs)
        else:
             self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

        self.model_run_metadata=run_metadata
        
        self.model.summary() # will print
        print('model (re)compiled elaT=%.1f sec'%(time.time() - start))
        

    #............................
    def train_model(self,args,extT,extV):
        XA=self.data['XA_train_'+extT] 
        XB=self.data['XB_train_'+extT]
        XC=self.data['XC_train_'+extT]
        Y= self.data['Y_train_'+extT]
        
        XA_val=self.data['XA_val_'+extV] 
        XB_val=self.data['XB_val_'+extV] 
        XC_val=self.data['XC_val_'+extV] 
        Y_val= self.data['Y_val_'+extV]
 
        if args.verb==0:
            print('train silently up to epochs:',args.epochs)
     
        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if args.earlyStopPatience>0:
            # spare: 
            earlyStop=EarlyStopping(monitor='val_loss', patience=args.earlyStopPatience, verbose=1, min_delta=0.003, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',args.earlyStopPatience)

        if args.checkPtOn:
            outF5w=self.outPath+'/'+self.name+'.weights_best.h5'
            chkPer=1
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=chkPer)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint, period=',chkPer)

        if args.reduceLearn:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=3, min_lr=0.0, verbose=1,epsilon=0.01)
            callbacks_list.append(redu_lr)
            lrCb=MyLearningTracker()
            callbacks_list.append(lrCb)  # just for plotting
            print('enabled ReduceLROnPlateau')

 
        print('\nTrain_model XA:',XA.shape, ' earlyStop=',args.earlyStopPatience,' epochs=',args.epochs,' batch=',args.batch_size)
        print('Train on bala=%s %d samples, validate on bala=%s %d samples'%(extT,len(Y),extV,len(Y_val)))
        startTm = time.time()
        verb=args.verb
        #verb=-1
        hir=self.model.fit([XA,XB,XC],Y, callbacks=callbacks_list,
                 validation_data=([XA_val,XB_val,XC_val],Y_val),  shuffle=True,
                 batch_size=args.batch_size, epochs=args.epochs, 
                           verbose=verb)
        fitTime=time.time() - start
        self.train_hirD=hir.history
        info={'train':(extT,len(Y)), 'valid':(extV,len(Y_val)),'batch':args.batch_size}
        self.train_hirD['lr']=lrCb.hir
        self.train_hirD['confusion']=lrCb.confusion
        self.train_hirD['info']=info

        loss=self.train_hirD['val_loss'][-1]
        acc=self.train_hirD['val_acc'][-1]
        acc0=self.train_hirD['val_acc'][0]
     
        print('\n Validation Accuracy:%.3f -->%.3f'%(acc0,acc), ', end Loss:%.3f'%loss,', fit time=%.1f min, epochs=%d'%(fitTime/60.,len(self.train_hirD['val_loss']))) 
        self.train_sec=fitTime
        self.acc=acc

        if args.tf_timeline:
             # Create the Timeline object, and write it to a json
             tl = timeline.Timeline(self.model_run_metadata.step_stats)
             ctf = tl.generate_chrome_trace_format()
             tlF=args.outPath+'/'+args.prjName+'.timeline.json'
             with open(tlF, 'w') as f: f.write(ctf)
             print('Saved TF timeline as',tlF,', point Chrome browser to : chrome://tracing/')



    #............................
    def save_model(self):
        outF=self.outPath+'/'+self.name+'.model.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('  closed  hdf5:',outF,' size=%.2f MB'%xx)


    #............................
    def save_species_split(self) :               
        outD={}
        for seg in  self.spec_split:
             outD[seg]={}
             for specN in self.spec_split[seg]:
                  coll=self.coll[specN]
                  outD[seg][specN]={'nHK':len(coll['HK']),'nRR':len(coll['RR']),'goodPair':coll['goodPair']}

        outF=self.dataPath+'/'+self.name+'.spec-split.yml'
        print('save species split as yaml:',outF)
        ymlf = open(outF, 'w')
        yaml.dump(outD, ymlf, Dumper=CDumper)
        ymlf.close()
        xx=os.path.getsize(outF)/1048576
        print('  closed  yaml:',outF,' size=%.2f MB'%xx)


    #............................
    def save_pairs_yaml(self,xobj,seg,ext):
        outF=self.dataPath+'/'+self.name+'.pairs-seg%d-%s.yml'%(seg,ext)
        #print('save pairs as yaml:',outF)
        ymlf = open(outF, 'w')
        #print('  yml-write:',seg,ext,type(xobj),len(xobj));
        yaml.dump(xobj, ymlf, Dumper=CDumper)
        ymlf.close()
        xx=os.path.getsize(outF)/1048576
        print('  closed  yaml:',outF,' size=%.2f MB'%xx)

    #............................
    def load_labeled_pairs_yaml(self,dom,ext):

        numTrainSeg=self.numSegm-2
        assert numTrainSeg>0  # makes no sense to split to train/eval/test 
        start = time.time()
        # this is nomnal, unwrapped range for test segments

        print('load_labeled_pairs_yaml dom=',dom,ext,'kfoldOffset=', self.kfoldOffset)
        n0=self.kfoldOffset
        wrkD={}

        if 'val'==dom:
            jL=[(n0+numTrainSeg)%self.numSegm] # one element

        if 'test'== dom:
            jL=[(n0+numTrainSeg+1)%self.numSegm] # one element

        if 'train' == dom:
            jL=[ (n0+j)%self.numSegm for j in range(numTrainSeg)]

        print('load_labeled segL:',jL,' as:',dom,ext)
        for k in jL:
            inpF=self.dataPath+'/'+self.name+'.pairs-seg%d-%s.yml'%(k,ext)
            print('load labeled pairs from yaml:',inpF,'...')
            ymlf = open(inpF, 'r')
            bulk=yaml.load( ymlf, Loader=CLoader)
            ymlf.close()
            for x in bulk:
                if x not in wrkD:
                    wrkD[x]=bulk[x]
                else:
                    wrkD[x]+=bulk[x]
                print ('  %s:%d,'%(x, len(bulk[x])),end='')
            assert len(wrkD)>0
            print('  ok , sum elaT=%.1f sec'%(time.time() - start))

        for x in wrkD:
            y=x.replace('-','_%s_'%dom)
            self.pairs_byname[y]=wrkD[x]

        assert len(self.pairs_byname) >0
        return dom+'_'+ext

    #............................
    def collect_species_info(self):
        specialL=[
              'Acidimicrobium ferrooxidans',
              'Geobacter',
              'Vibrio cholerae',
              'Escherichia coli',
              'Myxococcus xanthus',
              'Caulobacter crescentus'
        ]
        out={}
        print('collect_species_info over %d sepcies'%(len(self.coll)))
        for specN in self.coll:
            skip=True
            for x in specialL: 
                 if x in specN: skip=False
            if skip: continue
            taxonN=self.coll[specN]['taxon']
            if taxonN not in  self.taxonL: taxonN='Other'
            out[specN]={'taxon':taxonN}
            for role in self.mr12:
                out[specN][role]=len(self.coll[specN][role])
            out[specN]['goodPair']=self.coll[specN]['goodPair']
            #print(out[specN])

        outF=self.dataPath+'/'+self.name+'.spec-info.yml'
        print('save species info:',outF)
        ymlf = open(outF, 'w')
        yaml.dump(out, ymlf, Dumper=CDumper)
        ymlf.close()
        xx=os.path.getsize(outF)/1048576
        print('  closed  yaml:',outF,' size=%.2f MB'%xx)
        return out

        
    #............................
    def model_predict_segment(self,segName):
        start = time.time()
        kf='' # here only one kflod is expected to be loaded
        XA=self.data['XA_'+segName] 
        XB=self.data['XB_'+segName] 
        XC=self.data['XC_'+segName] 
        S=self.data['S_'+segName]

        y_true=self.data['Y_'+segName]

        print('model_predict_segment :%s  Y-size=%d'%(segName,len(y_true)))
        y_score = self.model[kf].predict([XA,XB,XC])
 
        specD=self.extract_ROC_per_speciec(zip(y_true, y_score,S))
        
        fpr, tpr, _ = roc_curve(y_true, y_score)
        roc_auc = float(auc(fpr, tpr))                       
        
        # this is wastefull - 2nd time computes score inside 'evaluate(..)'
        score = self.model[kf].evaluate([XA,XB,XC],y_true, verbose=0)
        loss= float(score[0])
        accuracy=float(score[1])
 
        nAll=y_true.shape[0]
        L1=int(y_true.sum())
        L0=nAll - L1
   
        print('  segment done, loss=%.3f, acc=%.3f, AUC=%.3f   elaT=%.1f sec'%(loss,accuracy,roc_auc,time.time() - start))

        sumD={'accuracy':accuracy,'AUC':roc_auc,'loss':loss,'nNeg':L0,'nPos':L1,'specD':specD}

        return sumD


        
    #............................
    def extract_ROC_per_speciec(self,yys_zip):
         #sort data by species
         specL={}
         outD={}
         for yt,ys,sp in yys_zip:
              if sp not in specL: specL[sp]={'y_true':[], 'y_score':[]}
              specL[sp]['y_true'].append(yt)
              specL[sp]['y_score'].append(ys)

         print('extract_ROC for %d species'%len(specL))
         # process each species separately
         for specN in specL:
              y_true=specL[specN]['y_true']
              y_score=specL[specN]['y_score']
              fpr, tpr, _ = roc_curve(y_true, y_score)
              roc_auc = float(auc(fpr, tpr))    
              L1=int(sum(y_true))
              L0=int( len(y_true) - L1)
              outD[specN]={'nNeg':L0,'nPos':L1,'auc':roc_auc}
              print('ROC:' ,specN,outD[specN])

         return outD
