import os, time,sys
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

from tensorflow.contrib.keras.api.keras.models import Model, load_model
#from sklearn.metrics import roc_curve, auc
from Util_Kinase import read_proteins_fasta as read_proteins_fasta

import datetime
import numpy as np
import yaml

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Oracle_Kinase(object):

    def __init__(self,args):
        self.name=args.prjName
        self.verb=args.verb
        print(self.__class__.__name__,', prj:',self.name)
        self.dataPath=args.dataPath
        self.outPath=args.outPath
        self.padChar='0'
        self.noiseFract=0.
        self.aminoacidL=list('ACDEFGHIKLMNPQRSTVWXY')
        self.aminoacidSet=set(self.aminoacidL)
        self.seqLenCut={'A':90,'B':130}
        self.mr12={'HK':'RR','RR':'HK'}
        self.runDate=datetime.datetime.now()
        self.taxonL= ['Actinobacteridae', 'Alphaproteobacteria', 'Bacillales', 'Betaproteobacteria', 'Clostridia', 'Cytophagia', 'Deltaproteobacteria', 'Epsilonproteobacteria', 'Flavobacteriia', 'Gammaproteobacteria', 'Lactobacillales', 'Sphingobacteriia', 'Spirochaetales']  # balanced good pairs, ver9c


        # - - - - - - - - - data containers - - - - - - - -
        # raw input from fasta
        self.protDB={} # [role][protN]{seq:AMINO,specN:geobac,assoc:Pair_A123, pair/fuzzy,taxon}
        self.taxonDB={} # [specN]{kingdom,phylum,class}
        # this is the primary specName reference (the names are shorter)
                
        # pruned and expanded input species list
        self.coll={} # [specN][role][protN]{seq,spencN,assoc,relation}
                     # [specN]['taxon','goodPair']

        # data divided by segment: (XA,XB,XC,Y) * (train,val,test,special)
        self.pairs_byname={} # [seg]{A:name,B:name,C:taxon,L:0/1}

        # 1-hot encoded data ready for training
        self.data={} # [XA[],XB[],XC[],Y[]]*[seg]
        
        # prepare 1-hot base for aminoacids:
        myDim=len(self.aminoacidL)
        self.oneHotBase={} # len=1+myDim, pad=00000000...
        i=0
        for x in self.aminoacidL:
            self.oneHotBase[x]=np.eye(myDim)[i]
            i+=1
        self.oneHotBase[self.padChar]=np.zeros(myDim)
        print('oneHot base, size=',len(self.oneHotBase), 'sample:')
        for x in ['0', 'A', 'C','Y']:
            print('base:',x,'1-hot:',self.oneHotBase[x].tolist())
        
        # prepare 1-hot taxon class
        myDim=len(self.taxonL)
        self.oneHotTaxon={}  # len=1+myDim  , 'Other'=000000....
        i=0
        for x in self.taxonL:
            self.oneHotTaxon[x]=np.eye(myDim)[i]
            i+=1
        self.oneHotTaxon['Other']=np.zeros(myDim)
        print('oneHot taxon, size=',len(self.oneHotTaxon), 'sample:')
        for x in [ 'Bacillales', 'Deltaproteobacteria', 'Other']:
            print('class:',x,'1-hot:',self.oneHotTaxon[x].tolist())

        print('Cnstr ready:%s\n'%self.name)
            
    #............................
    def expand_species(self,specN):
        # find species-class
        taxonD=None

        for x in self.taxonDB:              
            if x not in specN  : continue
            taxonD=self.taxonDB[x]
            break
        if taxonD==None:
            print('no taxonN for specN=',specN,' skip1')
            assert 34==56  # should quit , input name  invalid
            
        taxonN=taxonD['class']
        nSoFar=len(self.coll)
        pr= self.verb>1 or nSoFar <5 or nSoFar%500==0 or specN=='Geobacter sulfurreducens PCA' 
        if pr:
            print('\nexpand: %d  spec=%s, taxonN=%s'%(nSoFar,specN,taxonN))

        coll={'taxon':taxonN}

        # collect all HK,RR proteins belongin to this 1 species
        for role in self.mr12:
            protD=self.protDB[role]
            coll[role]={}
            for x in protD:
                if specN not in protD[x]['spec']: continue
                coll[role][x]=protD[x]
            #print('found %d %s'%(len(coll[role]),role))

        if len(coll['HK']) <1 or len(coll['RR']) <1:
            if pr:
                print('missing HKs or RRs , specN=',specN,', skip2');
            return  0,0,0

        # expand associations for each protein
        cnt2={}
        for role in self.mr12:
            #print('exa',role)
            role2=self.mr12[role]
            cnt={'orphan':0, 'pair':0,'fuzzy':0,'any':0}
            for protN in coll[role]:                
                if '_'+role2 in protN: # deal with RR_HK_Hybrid
                    relation='fuzzy'; pairN=None
                else:
                    assocStr=coll[role][protN]['assoc']      
                    relation,pairN=self.unpack_associations(assocStr,coll[role2])

                #print(role,protN,'-->',role2,pairN,relation,assocStr)
                coll[role][protN][relation]=pairN                
                #if relation=='pair':print(cnt['pair'],role,protN,'-->',role2,pairN)                
                # monitor
                cnt[relation]+=1
                cnt['any']+=1
                
            nColl=len(coll[role])
            if pr: print('%s-%d proteins '%(role,nColl),cnt)
            
            cnt2[role]=cnt['pair']
            # end of role

        if cnt2['HK']!=cnt2['RR'] : print('  Warn:  pair cnt not agree, correct it , specN=',specN,cnt2)
        
        self.crosscheck_pairs(coll)        
        
        # count again
        for role in self.mr12:
            cnt={'orphan':0, 'pair':0,'fuzzy':0}
            for protN in coll[role]:
                for x in cnt:
                    if x in coll[role][protN]:cnt[x]+=1
            cnt2[role]=cnt['pair']
            if self.verb >1:
                print('fin',role,cnt)
            
        assert cnt2['HK']==cnt2['RR'] 
        coll['goodPair']=cnt2['RR'] 
        self.coll[specN]=coll

        return  cnt2['HK'], len(coll['HK']) , len(coll['RR'])  # good pairs, nHK, nRR, only for accounting


    #....................
    def unpack_associations(self,assStr,protDB): # used by expand_species()
        #print('gcsA', assStr)
        if 'Orphan' in assStr:
            return 'orphan', None
            
        if 'Pair' not in assStr:
            return 'fuzzy', None

        # must be Pair, still can be a not matched Pair
        shortN=assStr[5:]
        #print('gcsB', shortN)
        outN=None
        for protN in protDB:
            #print('try',x,protN)
            if shortN not in protN: continue
            outN=protN
            break
        #print('gcsC', outN)
        if outN:
            return 'pair',outN
            
        return 'fuzzy', None

    
       
    #............................
    def crosscheck_pairs(self,coll) :  # used by expand_species()
        for role in self.mr12:
            role2=self.mr12[role]
            protD=coll[role]
            protD2=coll[role2]
            #print('crc',role,len(protD),len(protD2))

            i=0
            ii=0
            for name1 in protD:
                if 'pair' not in  protD[name1]: continue
                # now x is pair
                name2=protD[name1]['pair']
                
                #print( i,name1,name2,name2 in protD2, 'pair' in protD2[name2])
                if not 'pair' in protD2[name2]  or protD2[name2]['pair']!=name1:
                    ii+=1
                    protD[name1].pop('pair')
                    protD[name1]['fuzzy']=None                    
                    print(ii,'drop pair:',name1,'-->',name2)
                    continue
                i+=1
            

    #............................
    def load_proteins_fasta(self,role,ver='P2CS-2015v4.5'):
        fName=self.dataPath+'/'+ver+'-'+role+'.fasta'
        protDBL=read_proteins_fasta(fName,self.aminoacidSet)
        protDB={}        
        
        # currating protein self-consistency 
        protName_set=set()
        protSeq_set=set()
        specName_set=set()

        #print(self.taxonDB)
        #print('aaa',len(self.taxonDB))

        duplName=0; duplSeq=0; noTaxon=0
        for [protN,specN,assoc,seq]  in  protDBL:
            if protN in protName_set: 
                duplName+=1 ; continue
            if seq in protSeq_set: 
                duplSeq+=1 ; continue
            #print( protN,specN protN,specN,assoc,seq)
            
            if specN not in self.taxonDB:
                noTaxon+=1;   continue
                
            protName_set.add(protN)
            protSeq_set.add(seq)
            specName_set.add(specN)
            protDB[protN] ={'spec':specN, 'seq':seq,'assoc':assoc} 
             
        self.protDB[role]=protDB

        print('  %s-duplicates pruning, left unique %d prot-names-seq, %d specNames;  drop: %d prot-names, %d prot-seq, %d noTaxon'%(role, len(protDB), len(specName_set),duplName,duplSeq,noTaxon))
        
 
    #............................
    def load_taxonomy(self,fname='P2CS_taxonomy.txt'):
        fName=self.dataPath+'/'+fname
        taxonCnt={}
        taxDB={}
        print('read ',fName)
        fp = open(fName, 'r')
        k=0
        for line in fp:
            line = line.rstrip()
            k+=1
            #print(line)
            if 'section=' in line: continue
            idx1=line.find('PHPSESSID=">')
            idx2=line.find('<br>')
            x1=line[idx1+12:idx2-4]
            x2=line[idx2+4:-5]
            
            idx3=x1.rfind('(')
            specN=x1[:idx3].strip()
            idx4=specN.rfind('plasmid')  # drop specN extension=plasmid
            if idx4>0: specN=specN[:idx4]            
 
            # special cases
            idx=specN.rfind('pyogenes')  #Streptococcus pyogenes 
            if idx>0 : specN=specN[:idx+8]

            specN=specN.strip()
            if specN  in taxDB : continue
            
            yL=x2.split('/')            

            #print(k,idx1,'specN=',specN,idx4) 
            #print(idx2,x2)             
            #print(yL,len(yL))

            if len(yL) <= 2 :  continue # species class is undefined
            out={}
            out['kingdom']=yL[0].strip() 
            out['phylum']=yL[1].strip()
            sclass=yL[2].strip()
            if sclass[-1]=='.' : sclass=sclass[:-1]
            if sclass=='Bacilli' : sclass='Bacillales'
            out['class']=sclass
         
            #print(out)            
            taxDB[specN]=out
            if sclass not in taxonCnt : taxonCnt[sclass]=0
            taxonCnt[sclass]+=1

       
        print('done, Taxonomy for nSpec=',len(taxDB),' taxon classes=',len(taxonCnt))
        self.taxonDB=taxDB

        return
        # some tests  -was abnormaly high count of pairs
        for x in taxDB:
            if 'Listeria monocytogenes' in x:
                print(x)

        # one time code to produce taxonomy category list
        taxonL=[]
        taxL0=sorted(list(taxonCnt.keys()))
        print('any taxon len:',len(taxL0))
        otherSpecCnt=0
        for x in  taxL0:            
            if taxonCnt[x] >30:
                taxonL.append(x)
                print(x,taxonCnt[x])
            else:
               otherSpecCnt+= taxonCnt[x]
            
        print('other spec',otherSpecCnt)
        print('nBig=',len(taxonL))
        print(taxonL)
    
        
    #............................
    def build_pairs_array(self): # any HK against any RR
        #meaning:   A=HK, B=RR, C=taxon_class
        X={'A':[],'B':[],'C':[]};Y=[]  # final string-data containers
        #1 S=[] # auxiliary , list of species for test & eval only

        nMatch=0
        for specN in self.coll:
            collHK=self.coll[specN]['HK']        
            collRR=self.coll[specN]['RR']        
            taxonN=self.coll[specN]['taxon']
            for hk in collHK:
                #print(collHK[hk])
                for rr in collRR: 
                    #print('tt',hk,rr,taxonN)
                    X['A'].append(hk)
                    X['B'].append(rr)
                    X['C'].append(taxonN)
                    match=0
                    if 'pair' in collRR[rr]:
                        if hk in  collRR[rr]['pair'] : match=1
                        if match==1: assert  rr in  collHK[hk]['pair'] 
                    Y.append(match)
                    #1 S.append(specN)
                    nMatch+=match
                
        colN='_spec'
        for ab in ['A','B','C']:    
            self.pairs_byname['X'+ab+colN]=X[ab]
        self.pairs_byname['Y'+colN]=Y
        #1 self.pairs_byname['S'+colN]=S
        print('Pair_array len=',len(Y),' nMatch=%d, '%nMatch,self.pairs_byname.keys())



    #............................
    def encode1hotBase(self,seqPad): # convert sequences to 1-hot 2-D arrays        
        hot2D=[]
        for y in seqPad:
            hot2D.append(self.oneHotBase[y])
        return np.array(hot2D).astype(np.float32)


    #............................
    def clamp_pad_1hot_pairs(self,args):
        print('\nclamp_pad_1hot_pairs',list( self.pairs_byname))
        start = time.time()        

        for seg in self.pairs_byname:
            nameL=self.pairs_byname[seg]
            #print('loop seg',seg)
            k=1
            maxEve=args.events*0.7
            if 'val' in seg: maxEve/=5 # for validation need 5x less data
            m=len(nameL)

            if maxEve>0 : #tmp
                if m>maxEve:  k=int(m/maxEve)                    

            if k>1 : 
                nameL=nameL[::k]
                print(' reduced num pairs from %d to %d for %s'%(m,len(nameL),seg))

            if seg[:2]=='S_' : # ........... just copy species name
                self.data[seg]=nameL
                continue 
            
            if 'Y_' in seg : # ........   labels
                self.data[seg]=np.array(nameL).astype(np.float32)
                continue
            
            # below are only long serieas for XA, XB, XC --> 1hot
            num_eve=len(nameL) 

            if seg[:3]=='XC_' : #. . . . . taxon name --> 1hot
                num_taxon=len(self.taxonL)
                # clever list-->numpy conversion, Thorsten's idea
                XhotAllC=np.zeros([num_eve,num_taxon],dtype=np.float32)
                nOther=0; nFound=0;ieve=-1
                for taxonN in nameL:
                    ieve+=1
                    if taxonN in self.taxonL:
                        taxonOne= self.oneHotTaxon[taxonN]
                        nFound+=1
                    else:
                        taxonOne=self.oneHotTaxon['Other']
                        nOther+=1
                    XhotAllC[ieve,:]=taxonOne[:]
                    #print('C',taxonN,ieve,taxonOne)
                self.data[seg]=XhotAllC
                print('  taxonL done:',seg,' oneHot shape',self.data[seg].shape,' taxon: nFound=',nFound,' nOther=',nOther)
                
                continue  #----------- end of taxon --> 1 hot

                    
            max_len=self.seqLenCut[seg[1]]
            num_amino=len(self.aminoacidSet)
            role='HK'
            if seg[1]=='B': role ='RR'
            print('    pad-1hot seg=%s pairs=%d role=%s'%(seg,num_eve,role))
            print('    aminoacid len<=',max_len,' of :',seg,' inpLen=',len(nameL))
            noiseMask=False
            if self.noiseFract>0 and '_over' in seg:
                noiseMask=True
                print('    masking input enabled, noiseFract=',self.noiseFract)

            # clever list-->numpy conversion, Thorsten's idea
            XhotAll=np.zeros([num_eve,max_len,num_amino],dtype=np.float32)

            n1=0; n2=0; ieve=-1
            for protN in nameL:
                seqStr=self.protDB[role][protN]['seq']
                #if protN=='ECP_2164_HK_Classic':
                #    print('ccc',seqStr[:20])
                ieve+=1
                m=len(seqStr)
                dm=max_len-m

                if dm >0 :  #add padding
                    seqStr=self.padChar*dm+seqStr
                    n2+=1   

                if noiseMask:
                    seqStr=self.addNoise(seqStr)

                if dm <0 :  # clip the end
                    seqStr=seqStr[:max_len]
                    n1+=1
                #print(dm,m,seqStr,n1,n2,' new m',len(seqStr)) 

                XhotOne=self.encode1hotBase(seqStr)
                XhotAll[ieve,:]=XhotOne[:]
                #print('AB',protN,ieve)

            self.data[seg]=XhotAll
            print('  proteinL done:',seg,' oneHot shape',self.data[seg].shape,' nPad=',n2,' nClip=',n1)
            
            #print('XX',XXhot.shape, self.data[seg].shape)
        print('ML data ready  elaT=%.1f sec :'%(time.time() - start),  self.data.keys())



    #............................
    def load_Kmodels(self,path='.',kL=['']): # for prediction only
        # expand list if '-' are present
        kkL=[]
        for x in kL:
            if '-' not in x: 
                kkL.append(x) ; continue
            xL=x.split('-')
            for i in range(int(xL[0]),int(xL[1])+1):
                kkL.append(i)
        #print(kL,'  to ',kkL)
        kL=kkL
        
        nK=len(kL)
        assert nK>0
        self.model={}
        runDateStr=self.runDate.strftime("%Y-%m-%d_%H.%M")
        self.info={'model':path,'kModelL':kL,'oracle':self.name,'runDate':runDateStr}

        print('load_kfold_models %d :'%nK,kL)
        start = time.time()
        for k in kL:
            inpF5m=path+'%s/'%k+self.name+'.model.h5'
            print('load %d model and weights  from'%len(self.model),inpF5m,'  ... ')
            self.model[k]=load_model(inpF5m) # creates mode from HDF5
            if len(self.model)==1 :
                self.model[k].summary()
            else:
                assert self.model[k].count_params() == self.model[kL[0]].count_params() 
        print('%d models loaded, elaT=%.1f sec'%(nK,(time.time() - start)))

    #............................
    def load_weights_4_retrain(self,path='.'):
        start = time.time()
        inpF5m=path+'/'+self.name+'.weights_best.h5'
        print('load  weights  from',inpF5m,end='... ')
        self.model.load_weights(inpF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))


    #............................
    def predict_one_species(self):
        seg='spec'
        assert len(self.coll)==1
        for specN in self.coll: break # need  just 1st and only element
        collD=self.coll[specN]
        taxonN=collD['taxon']
        nHK=len(collD['HK'])
        nRR=len(collD['RR'])
        nGoodPair=collD['goodPair']
        
        print('predict_one_species:',specN,', HK=',nHK,' RR=',nRR,' nGoodPair=',nGoodPair)
        XA=self.data['XA_'+seg] 
        XB=self.data['XB_'+seg]
        XC=self.data['XC_'+seg] 
        Ytrue=self.data['Y_'+seg]
        
        HK=self.pairs_byname['XA_'+seg] 
        RR=self.pairs_byname['XB_'+seg] 

        #... step-1 predict all HK*RR pairs at once
        nK=len(self.model)
        print('  input num pairs=%d using kfold=%d models'%(len(XA),nK))
        start = time.time()

        Yscore=np.array(0)  # accumulator
        for k in self.model:
            Yscore=np.add(Yscore,self.model[k].predict([XA,XB,XC]).flatten())
            print(k,' kfold done, score[0].sum=',Yscore[0])
        print('   prediction for %s done,  elaT=%.1f sec'%(seg,(time.time() - start)))
        # renormalize score if multi-model predictionw as made
        if nK>1:
            Yscore=np.multiply(Yscore, 1./nK)

        if 0: # FAKE predictions
            fake=[ np.random.uniform() for x in range(len(XA)) ]
            print('fake pred, sample:',fake[:10])
            Ypred=np.array(fake)
        
        # outgoing record structure:
        #out2d[rr][pred]{nameHK:val}
        #         [truth]{orphan/fuzzy or pair:name }
        
        #... step-2 re-pack output by HK-RR names
        out2d={}
        nTruePair=0
        for yt,yp,hk,rr in zip(Ytrue,Yscore,HK,RR):
            if rr not in out2d:
                rec=collD['RR'][rr]
                val=None
                if 'pair' in rec :
                    val={'pair': rec['pair']}
                    nTruePair+=1
                elif 'orphan' in rec : val='orphan'
                elif 'fuzzy' in rec : val='fuzzy'

                out2d[rr]={'truth':val, 'pred':[]}                
                # RR-row initialized
                
            out2d[rr]['pred'].append( [hk,float(yp)]) #prediction

        #print('mm', nTruePair,nGoodPair)        
        assert nTruePair==nGoodPair

        #... step-4   score-sort HKs for fixed RR, repack output as a list
        for rr in out2d:
            dd=out2d[rr]['pred']
            #print('\n***dd0',dd)
            dd1=sorted(dd,key=lambda x: x[1], reverse=True)
            #print('***dd1',dd1)
            out2d[rr]['pred']=dd1
        
        out={'matrix':out2d, 'spec':specN, 'taxonTrue':taxonN,'truePairs':nTruePair,'HK':nHK,'RR':nRR,'info':self.info}
        if taxonN in self.taxonL:
            out['taxonTrain']=taxonN
        else:
            out['taxonTrain']='Other'


        coreName = "_".join( specN.split() )
        coreName = coreName.replace('/','Slash')
        coreName = coreName.replace('*','Star')
        coreName = coreName.replace('.','Dot')
        coreName = coreName.replace(':','Colon')
        coreName = coreName.replace('=','Is')
        coreName = coreName.replace('(','Left_')
        coreName = coreName.replace(')','_Right')
        coreName =coreName[0].upper()+coreName[1:] # rarely name starts with a small letter

        outF=self.outPath+'/'+coreName+'.score_matrix.yml'
        with open(outF, 'w') as outfile:
            yaml.dump(out, outfile)
            #print ('  AUC=%.3f, saved species: %s'%(roc_auc,outF)),, 'roc':rocD


    #............................
    def Xcount_pairs(self,specN):   #drop this function later
        if specN not in self.coll: return -1
        inpD=self.coll[specN]['HK']
        nPair=0
        for protN in inpD:
            if 'pair' in inpD[protN] : nPair+=1
        return nPair
