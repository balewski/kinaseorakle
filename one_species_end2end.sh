#!/bin/bash
# do either predictions or plots for one species (Geobacter) 
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value
#env|grep SLURM

oraId=59  # location of matrix.yml
#specN=Acidimicrobium_ferrooxidans_DSM_10331
#specN=Caulobacter_crescentus_CB15
#specN=Escherichia_coli_536
specN=Geobacter_sulfurreducens_PCA
#specN=Myxococcus_xanthus_DK_1622
#specN=Vibrio_cholerae_M66-2
#oraId=51 ;

#scorePath=/global/cscratch1/sd/balewski/kinase10_sum/10b/kinase10b-ora-${oraId}/scores/
scorePath=/global/cscratch1/sd/balewski/kinase10c-oraB-5/scores/
#scorePath=scores/
plotPath=plots1
mkdir -p ${plotPath}/givenRR
mkdir -p ${plotPath}/givenHK
mkdir -p ${plotPath}/data
chmod a+r -R  ${plotPath}
chmod a+x -R  ${plotPath}
echo made out dir ${plotPath}


echo    
echo " ==================  work on specN=$specN   $scorePath  "
./recomOne_Kinase.py --scorePath $scorePath  --specName $specN --plotPath $plotPath  -X  # --do-PDF
#    ./recomOne_Kinase.py --scorePath $scorePath  --specName $specN --plotPath $plotPath  -X --given HK
    
