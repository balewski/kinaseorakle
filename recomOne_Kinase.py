#!/usr/bin/env python
""" 
read yaml predictions for a species and  recommend topN  proteins
 Can do either
- given RR recommend top HK or
- given HK recommend top RR

prerequisits:
- must exist output :  
     mkdir -p plots/givenRR/
     mkdir -p plots/givenHK/
     mkdir -p plots/data

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from RecomOne_Kinase import RecomOne_Kinase

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--scorePath",
                        default='scores/',help="path to input socres per species")

    parser.add_argument('-g',"--given", choices=['HK','RR'],
                        default='RR',help="fixed protein role")

    parser.add_argument('-n',"--specName", 
                        default='fixMe1',help="species name with '_' ")

    parser.add_argument("--plotPath",
                        default='plots/',help="output path for plots")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument('--venue', dest='venue', choices=['web','icml'],
                         default='web',
                         help="change fromat of output plots to match publisher requirements")

    args = parser.parse_args()
    args.prjVersion='10'
    args.prjName='kinase'+args.prjVersion # core name used everywhere
    args.deepName='Kinase Oracle '+args.prjVersion+'d'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

#specName='Anoxybacillus_flavithermus_WK1'
#args.specName='Acidimicrobium_ferrooxidans_DSM_10331'
#args.specName='Geobacter_sulfurreducens_PCA' # 'known pairs': 33, 'HK': 89, 'RR': 115


recom=RecomOne_Kinase(args, args.specName )

recom.study_one_zachQ1('ECP_2163_RR_LytTR'); exit(0) # 2018-08 discussion, zach Q1

recom.loop_givens()
recom.plot_roc(10)
recom.plot_score_heatmap(12)
recom.plot_labeled_scores(13)
recom.plot_popularity(11)
recom.save_summary()
recom.coverHTML()
