#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='read yaml predictions for one species and explore it',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i","--input", required=True,
                        help="input prediction matrix (YAML) ")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

 
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

with open(args.input, 'r') as fd:
    specD=yaml.load( fd)
    print ('loaded %d RR predictions from:%s'%(specD['RR'],args.input))

out2d=specD['matrix']

for rr in out2d:
    try:
        trueHK=out2d[rr]['truth']['pair']
    except:
        trueHK=out2d[rr]['truth']

    dd=out2d[rr]['pred']
    topK_L=[ (x, dd[x] ) for x in sorted(dd, key=dd.get, reverse=True)]

    print('\n- - - - - -  sorted matches for RR=',rr)
    nHK=len(topK_L)
    for i in range(nHK):
        hk,score= topK_L[i]
        trStr=''
        if trueHK==hk : trStr=' TRUTH'
        print('rank=%d %s  score=%.4g %s'%(i,hk,score,trStr))
