import numpy as np
import os
import shutil
import datetime
import yaml
from matplotlib.colors import LogNorm
from matplotlib import cm as cmap
import matplotlib.ticker as ticker
from pprint import pprint
from sklearn.metrics import roc_curve, auc

from Util_Kinase import getGeneAddress as getGeneAddress
from Util_Kinase import createPubDir as createPubDir

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"



#............................
#............................
#............................
class RecomOne_Kinase(object):

    def __init__(self, args,coreName):  
        self.name=coreName
        self.deepName=args.deepName
        path0=args.plotPath+'/given'+args.given
        self.pubPath=createPubDir(path0,coreName)+'/'
        self.given=args.given
        self.venue=args.venue
        self.recom='HK' 
        if self.given=='HK':self.recom='RR' 
        
        self.imgForm='png'
        self.doRRHKplots=True  # disable for testing
        if args.venue!='web' :  self.imgForm='pdf'

        self.prtCnt=0 # only for progress monitor
        self.my248={2:None,4:None,8:None}  
        self.set_topN=8 # here define the how many topN will be considered
        self.outRec={}; 

        self.outRecL=['known pairs'] 
        for n in self.my248:
            tit='true%s inTop%d'%(self.recom,n)
            self.my248[n]=tit
            self.outRec[tit]=0
            self.outRecL.append(tit)
        self.outRecL+=['topK_95 percentile','avrTrue rank','AUC']
        
        inpF=coreName+'.score_matrix.yml'
        inpFF=args.scorePath+'/'+inpF
 
        with open(inpFF, 'r') as fd:
            self.specD=yaml.load( fd)
            print ('loaded %d %s scores arrays from: %s'%(self.specD[self.given],self.given,inpFF))
        
        self.relDataPath='../../data/'
        self.yamlData=self.relDataPath+inpF   
 
        ''' 2018-08 : if yourt code crashes here, remember the following dirs must exist:
        plotPath=plots
        mkdir -p $plotPath/givenRR/taxons
        mkdir -p $plotPath/givenHK/taxons
        mkdir -p $plotPath/data
        '''

        outFF=self.pubPath+self.relDataPath+inpF
        #print('ff',inpF, '\nfullInp:',inpFF,'\nfullOut:',outFF)

        shutil.copy(inpFF,outFF)
        #os.chmod(outFF,0o764) # a+r 
        os.chmod(outFF,0o644) # -rw-r--r--                 

        if self.specD[self.recom] <self.set_topN:
            print('TMP %s skip due to n%s=%d'%(self.name,self.recom,self.specD[self.recom]))
            exit(0)
            return

        
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server

        import matplotlib.pyplot as plt
        self.plt=plt


    #............................
    def transpose_2Dmatrix(self): # in place!
        out2d=self.specD['matrix']
        print('transpose_2dmatrix inp ') # pprint(out2d)
        new2d={}
        truthL={}
        for rr in out2d:            
            for hk,score in out2d[rr]['pred']:
                #print('a1',rr,hk)
                new2d[hk]={'pred':[],'truth':'orphan'}
            break # need just one valid list
        # now HK lables are primary 
        #pprint(new2d)
        
        for rr in out2d:
            truthL[rr]=out2d[rr]['truth']
            for hk,score in out2d[rr]['pred']:                
                new2d[hk]['pred'].append([rr,score]) 
        # 2D matrix populate dbut not score-sorted
        #pprint(new2d)
        # score-sort RRs for fixed HK, repack output as a list
        for hk in new2d:
            dd=new2d[hk]['pred']
            dd1=sorted(dd,key=lambda x: x[1], reverse=True)
            new2d[hk]['pred']=dd1
 
        # copy pairs
        for rr in truthL:
            if 'pair' in truthL[rr]:
              new2d[truthL[rr]['pair']] ['truth']= {'pair':rr} 
        #pprint(new2d)
        self.specD['matrix']=new2d

    #............................
    def loop_givens(self):               

        self.figId=100
        self.pltName1=[]
        self.pltName2=[]
        self.trueRankL=[]
        self.popular={} # for popularity plot
     
        if self.given=='HK': 
            self.transpose_2Dmatrix() # in place!

        out2d=self.specD['matrix']
        ytV=[]; ypV=[]
        for giv in out2d:
            #print('GG',giv)
            self.figId+=1
            try:
                trueMatch=out2d[giv]['truth']['pair']
            except:
                trueMatch=out2d[giv]['truth']
            self.recom_one( giv,trueMatch,out2d[giv])            
            
            #if self.figId>=102 :break
        
        xx=np.array(self.trueRankL)
        if len(self.trueRankL)>0:
            avr_hk_rank=xx.mean()
            std_hk_rank=xx.std()
            avr_hk_str="%.1f +/- %.1f"%(avr_hk_rank,std_hk_rank)
            percent95=np.percentile(xx,95)
            #print(' avr_hk_rank=%s'%avr_hk_str, ' 95 percentile=',percent95)
        else:
            percent95=-1
            avr_hk_str='n/a'

        # compute AUC of ROC
        ytV=[]; ypV=[]
        for giv in out2d:
            if 'pair' not in out2d[giv]['truth']: continue
            trueMatch=out2d[giv]['truth']['pair']
            recoV=out2d[giv]['pred']
            for recomN,score in recoV:
                ypV.append(score)
                ytV.append(  int(recomN==trueMatch))

        if self.specD['truePairs'] <10:
            print('no AUC calculation due to low count of  nPairs=%d'%(self.specD['truePairs']))
            self.rocD={'auc':0}
            self.outRec['AUC']='n/a'
        else:
            fpr, tpr, _ = roc_curve(ytV,ypV)
            roc_auc = float(auc(fpr, tpr))                       
            print(self.name,'given=%s AUC: %.3f  size=%d  len(fpr)=%d  truePairs=%d' %( self.given,roc_auc,len(ytV),len(fpr),self.specD['truePairs']))
            self.rocD={'auc':roc_auc,'inp_size':len(ytV),
                       'fpr':fpr.tolist(),'tpr':tpr.tolist() }
            self.outRec['AUC']='%.3f'%roc_auc
        
        self.outRec['avrTrue rank']=avr_hk_str
        self.outRec['topK_95 percentile']='%.1f'%percent95
        self.outRec['known pairs']=len(xx)
        self.outRec['HK']=self.specD['HK'] 
        self.outRec['RR']=self.specD['RR'] 
                    
    #............................
    def recom_one(self,givenName,trueMatchN, scoreD):
        topK_L=scoreD['pred']

        #print('mm given=',givenName)
        #pprint(topK_L)
        rankTrue=-1
        probV=[]; nameV=[]
        nScore=len(topK_L)
        for i in range(nScore):
            recomN,score=topK_L[i]

            #if i==0: print('top0=',hk)
            # for popularity plot
            if i < self.set_topN :
                if recomN not in self.popular: self.popular[recomN]=[]
                self.popular[recomN].append(i)

            if trueMatchN==recomN :
                rankTrue=i
                scoreD['truth']['rank']=rankTrue
                self.trueRankL.append(rankTrue)
                for n in self.my248:
                    if rankTrue<n: self.outRec[ self.my248[n] ]+=1
                recomN='cognate  '+recomN
                
            if i >= self.set_topN : continue
            probV.append(score)
            nameV.append(recomN)
            
            
            #print(i,hk,rank)
        #print(' given=%s : %s,  trueRank=%d'%(givenName,trueMatchN,rankTrue))
        #print('qq',scoreD)
        #print('len',len(probV),probV)
        
        if self.doRRHKplots:
            self.plot_one_topN(givenName,trueMatchN,rankTrue, probV,nameV)
        
        return  probV,nameV  # used only for some tests
        
    #............................
    def plot_one_topN(self,givenName,trueMatchN,rankTrue, probV,nameV):
        nChoice=self.specD[self.recom]
        
        xyIn=3.5
        if self.venue!='web' : xyIn=3.
        self.plt.figure(self.figId,facecolor='white', figsize=(xyIn,xyIn))
        
        ax=self.plt.subplot(1,1, 1)

        ax.barh(range(len(nameV)), probV, align='center',color='peachpuff')
        ax.invert_yaxis()

        txtSZ=20
        ax.set_yticks(range(len(nameV)))
        ax.set_yticklabels(nameV, size=txtSZ/2,x=0.96)
        
        genAdd=getGeneAddress(givenName) 
        ax.set(xlabel='pair score given %d%s'%(genAdd,self.given),ylabel='top-%d  matches of %d %s'%(self.set_topN,nChoice,self.recom))
        ax.set_xlim([0.,1.03])
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter('%.1f'))

        if  self.venue=='web':  
            ax.set_title(givenName) 
            ax.axvline(x=0.5,linewidth=1, color='salmon', linestyle='--')
            ax.axvline(x=0.8,linewidth=1, color='salmon', linestyle='--')
            if rankTrue<0 : ax.text(0.1,4, trueMatchN,rotation=90)

        if rankTrue>=self.set_topN : 
            ax.text(0.05,2,'true HK rank=%d'%rankTrue,rotation=90)
            ax.text(0.15,0,trueMatchN,rotation=90)
            ax.text(0.97,0.01,specN.replace('_',' '),rotation=90, size=10, verticalalignment='top')
        specN=self.name

        self.plt.tight_layout()
        
        coreFig='fig%d_%s'%(self.figId,givenName)
        self.save_plot( coreFig )
        self.pltName2.append(coreFig)
        
     #......................
    def save_plot(self, coreFig ):
        figName='%s/%s.%s'%(self.pubPath,coreFig,self.imgForm)
        self.prtCnt+=1
        if self.prtCnt <5 or self.prtCnt%20==0 :
            print('Saving  %s ...'%figName)
        self.plt.savefig(figName)
        self.plt.close()
        os.chmod(figName,0o644) # -rw-r--r-- 

      
    #............................
    def plot_popularity(self,figId):  # aka: master regulator 
        vX=[]
        vY2=[]; vY4=[]; vY8=[]

        for recomN in sorted(self.popular):
            vX.append(recomN)
            n2=0;n4=0;n8=0
            for rank in self.popular[recomN]:
                if rank<2 : n2+=1
                if rank<4 : n4+=1
                if rank<8 : n8+=1
               
            vY2.append(n2)
            vY4.append(n4)
            vY8.append(n8)
            
        nX=len(vX)
        assert nX >1
        xIn=5
        if nX>30 : xIn=nX/6
        
        self.plt.figure(figId,facecolor='white', figsize=(xIn,4))        
        ax=self.plt.subplot(1,1, 1)

        ax.plot(range(nX), vY2,'o',color='blue', markersize=6, label='top2');
        #ax.plot(range(nX), vY8,'D',color='red',markerfacecolor='none', markersize=7, label='top8');

        ax.bar(range(nX), vY4, width=0.90, align='center',color='plum', label='top4');

        ax.yaxis.grid()  # horizontal lines

        nGiv=self.specD[self.given] 
        nRec=self.specD[self.recom] 
        txtSZ=20
        ax.set_xticks(range(nX))
        ax.set_xticklabels(vX, rotation='vertical', y=0.89, size=txtSZ/2)
        ax.set(xlabel='%s candidate'%self.recom,ylabel='num %s matches of %d'%(self.given,nGiv))
        ax.set_title('%s popularity, %s'%(self.recom,self.name),size=10)
        ax.legend( loc='upper left', fontsize=txtSZ*.7)
   
        ax.set_ylim(0.,max(vY4)*1.05)
        ax.set_xlim(-1,nX+1)
        
        Npop=2*nGiv/nRec
        ax.plot([0, nRec], [Npop, Npop], linewidth=1, color='blue', linestyle='--')
        #Npop*=2
        #ax.plot([0, nRec], [Npop, Npop], linewidth=1, color='red', linestyle='--')
        Npop*=2
        ax.plot([0, nRec], [Npop, Npop], linewidth=1, color='darkorchid', linestyle='--')
 

        self.plt.tight_layout()        
        coreFig='fig%d_master_regulator'%(figId)
        self.save_plot( coreFig )
        self.pltPopular=coreFig

      
    #............................
    def plot_roc(self,figId):
        inp=self.rocD
        if inp['auc']<=0:            
            print('ROC not computed, skip plotting')
            return
        roc_auc=inp['auc']
        inp_size=inp['inp_size']
        fpr=inp['fpr']
        tpr=inp['tpr']
        
        #print('my auc=',roc_auc,len(fpr),' inp size=',inp_size, ' given=',self.given)
        yIn=4.
        if self.venue!='web' : yIn=3.
        self.plt.figure(figId,facecolor='white', figsize=(1.05*yIn,yIn))
        
        ax=self.plt.subplot(1,1, 1)
        color='seagreen'

        ax.plot(fpr, tpr, label='ROC',color=color )

        nHK=self.specD['HK']
        
        ax.plot([0, 1], [0, 1], 'k--', label='coin flip')
        ax.plot([0], [1], linestyle='None', marker='+', color='magenta', markersize=15,markeredgewidth=2, label='ideal classifier')
        ax.set(xlabel='false positive rate',ylabel='true positive rate')
        tit=''
        if self.venue=='web':
            ax.set_title('ROC:'+self.name,size=10)
            ax.text(0.05,0.45,'given %s recomend %s'%(self.given,self.recom))
            ax.text(.57,1.015,self.name,horizontalalignment='right')
            ax.grid(color='brown', linestyle='--',which='both')
            tit='AUC = %0.3f' % roc_auc

        ax.legend(loc='center right', title=tit)
        ax.set_ylim(0.4,1.05) ;   ax.set_xlim(-0.05,0.6) 

        

        self.plt.tight_layout()

        coreFig='fig%d_AUC'%(figId)
        self.save_plot( coreFig )
        self.pltName1.append(coreFig)


    #............................
    def plot_labeled_scores(self,figId):
        out2d=self.specD['matrix']

        self.plt.figure(figId,facecolor='white', figsize=(4.5,3))  
        ax=self.plt.subplot(1,1, 1)

        score_thr=0.90
        u={0:[],1:[],'u':[]}
        m={0:0,1:0,'u':0}
        rr1=0; rru=0
        for rr in out2d:            
            if 'pair' in out2d[rr]['truth']:
                trueHK=out2d[rr]['truth']['pair']
                rr1+=1
            else:
                trueHK=None
                rru+=1
            topHK_L=out2d[rr]['pred']
            for hk,score in topHK_L:
                if trueHK==None:
                    ygt='u'
                else:
                    ygt=trueHK==hk
                u[ygt].append(score)
                if score >score_thr: 
                    m[ygt]+=1
                    #if ygt=='u' : print(ygt,'unkn',rr,hk,'%.3f'%score)
                    if self.venue!='web' and  ygt=='u' :
                        print('%s & %s & %.3f \\\\'%(rr.replace('_','\_'),hk.replace('_','\_'),score))

        '''
        # testing dimensions, can be ignored
        print('ww', len(u[0]),len(u[1]),len(u['u']))
        iRR=len(out2d)
        for rr in out2d:
            iHK=len(out2d[rr]['pred'])
            break
        iGT=len(u[1])
        nAll=iHK*iRR
        nUn=(iRR-iGT)*iHK
        print('iRR',iRR,'iHK',iHK,'nAll',nAll,'iGT',iGT,'nUn',nUn) 
        '''

        bins = np.linspace(0.0, 1., 50)
        ax.hist(u[1], bins, alpha=0.5,label='%d cognate pairs out of  %d'%(m[1],len(u[1])))
        ax.hist(u[0], bins, alpha=0.5,label='%d not-cognate pairs out of %d'%(m[0],len(u[0])))
        ax.hist(u['u'], bins, alpha=0.5,label='%d unknown pairs out of %d'%(m['u'],len(u['u'])),histtype='step',color='black',lw=2)
        ax.axvline(x=score_thr,linewidth=2, color='blue', linestyle='--')

        ax.set(xlabel='pair score given %s'%(self.given), ylabel='num pairs')

        ax.set_yscale('log')
        
        if  self.venue=='web': 
            ax.set_title('Predictions for labeled pairs')
            ax.grid(True)
            ax.text(1.01,1.05,self.name,rotation=90, verticalalignment='bottom')

        ax.legend(loc='upper center', title='score thr > %.2f'%score_thr)
        self.plt.tight_layout()

        coreFig='fig%d_labeled_scores'%(figId)
        self.save_plot( coreFig )
        self.pltName1.append(coreFig)


            
    #............................
    def plot_score_heatmap(self,figId):
        # note, the names are as for given=RR but should work both ways
        
        topK=4
        if self.venue!='web': topK=2
 
        out2d=self.specD['matrix']
        rrL=sorted(out2d)

        # reorder list-1 by  gene index on the genome
        dd={ x:getGeneAddress(x) for x in rrL }
        rrL=[ x  for x in sorted(dd, key=dd.get, reverse=False)]
        rrA=dd  # for display as short lables

        nrr=len(rrL)
        #print(nrr,'rrL=',rrL)

        hkL=sorted([ list(x)[0] for x in out2d[rrL[0]]['pred'] ])
        nhk=len(hkL)
        # reorder list-2 by  gene index on the genome
        dd={ x:getGeneAddress(x) for x in hkL }
        hkL=[ x  for x in sorted(dd, key=dd.get, reverse=False)] 
        hkA=dd  # for display as short lables
       

        #print(nhk,'hkL=',hkL)
        topK=min(topK,nhk)

        xIn=max(10,  nrr/6.)
        yIn=max(6,nrr/6.)
        mkSize=70/pow(yIn,0.6)

        if self.venue!='web':
            xIn=9;  yIn=7
        fig=self.plt.figure(figId,facecolor='white', figsize=(xIn,yIn))
        ax=self.plt.subplot(1,1, 1)

        # build 2D score map for top 2
        scoreMap=np.zeros((nhk,nrr))

        #scoreX=[]; scoreY=[]
        trHK=[]; trRR=[]
        for irr in range(nrr):
            if 'pair' in out2d[rrL[irr]]['truth']:
                hk=out2d[rrL[irr]]['truth']['pair']
                ihk=hkL.index(hk)
                trHK.append(ihk)
                trRR.append(irr)            
            hkPred=out2d[rrL[irr]]['pred']
            for j in range(topK):
                hk,score=hkPred[j]
                ihk=hkL.index(hk)
                #score=0.
                scoreMap[ihk,irr]=score

        #print('found %d pairs'%len(trHK))

        #plot it, self.plt.cm.Reds

        # mask some 'bad' data, in your case you would have: data == 0
        scoreMap = np.ma.masked_where(scoreMap < 0.05, scoreMap)
        #cmap.cool.set_under(color='black')  cmap.OrRd
        res=ax.imshow(scoreMap,cmap=cmap.cool,origin='lower')#, interpolation='none')
        
 
        mkColor='blue'
        ax.plot(trRR,trHK, marker='o', linestyle='None',markerfacecolor='None',  markersize=mkSize,color=mkColor, markeredgewidth=2)

        if self.venue!='web':
            rrL2=['%d%s'%(rrA[k],self.given) for k in rrA]
            hkL2=['%d%s'%(hkA[k],self.recom) for k in hkA]
        else:
            rrL2=rrL
            hkL2=hkL            
            ax.yaxis.grid()

        ax.xaxis.grid()

        tickV=np.arange(nrr)
        ax.set_xticks( tickV)
        ax.set_xticklabels(rrL2, rotation=90)


        tickV=np.arange(nhk)
        ax.set_yticks( tickV)
        ax.set_yticklabels(hkL2)
        ax.set(xlabel='given %s protein'%self.given,ylabel='recommended %s protein'%(self.recom))
        fntSize=20
        ax.xaxis.label.set_size(fntSize); ax.yaxis.label.set_size(fntSize)

        if self.venue=='web':
            ax.text(0,-2,'blue circle=TRUTH   ',color='b',ha='right')
            ax.text(0,-4,'Z axis=pred score   ',color='brown',ha='right')
            ax.set_title('top-%d %s given %s,  %s'%(topK,self.recom,self.given,self.name))
            

        if self.imgForm=='pdf':
            import matplotlib.patches as patches
            ax.add_patch(
                patches.Rectangle(
                    (5.5, 40.5),   # (x,y)
                    0.95,          # width
                    39.9,          # height
                    #alpha=0.3,
                    #fill=False,
                    facecolor="lime"
                )
            )

        
        #print('nx=',nrr,'ny=',nhk)
        self.plt.tight_layout()
        # fig.colorbar(res)  $shows z-axis

        coreFig='fig%d_2DtopK%d'%(figId,topK)
        self.save_plot( coreFig )
        self.plt2DtopK=coreFig

    #----------------
    def save_summary(self):
        self.yamlSum='summary.yml'
        outF=self.pubPath+self.yamlSum
        #print('qq',self.name,recL,outF)
        sumD={'Taxonomy class':self.specD['taxonTrain'], 'Species':self.name,	'HK':self.specD['HK'],	'RR':self.specD['RR'],'given':self.given}
        for x in self.outRecL:
            sumD[x]=self.outRec[x]
        #print('sumD=',sumD)
        ymlf = open(outF, 'w')
        yaml.dump(sumD, ymlf)
        ymlf.close()
        os.chmod(outF,0o644) # -rw-r--r-- 
        
            
    #----------------
    def coverHTML(self):
        outF=self.pubPath+'/index.html'

        dateStop=datetime.datetime.now()
        dateNowStr=dateStop.strftime("%Y-%m-%d_%H.%M")
        recL=self.outRecL[:-1]
        
        print(self.name,self.outRec)
        SP3='&nbsp; &nbsp; &nbsp;'
        SP2='&nbsp; &nbsp;'
        with open(outF,'w') as fd:
            fd.write("<html>\n<head></head>\n<body>\n")
            fd.write('\n<p>%s , produced: '%self.deepName+dateNowStr)
            
            fd.write('\n<br>species: <b> %s </b> '%self.name+SP3)
            fd.write('class: <b> %s </b> '%self.specD['taxonTrue']+SP3)
            fd.write('HK: <b> %s </b>  '%self.specD['HK']+SP2)
            fd.write('RR: <b> %s </b> '%self.specD['RR']+SP2)
            fd.write('true pairs: <b> %s </b> '%self.specD['truePairs']+SP3)
            roc_auc=self.rocD['auc']
            fd.write('AUC: <b> %.3f </b> given %s '%(roc_auc,self.given+SP3))
            #print('yyy',self.yamlData)
            fd.write('\n YAML <a href="%s"> matrix </a>  , '%(self.yamlData))
            fd.write('\n  <a href="%s"> summary </a>  <br>'%(self.yamlSum))

            for x in recL:                
                fd.write(' %s : <b> %s</b> '%(SP3+x, self.outRec[x]))
            
            self.print_recom_table(fd)

            self.addAllPlots2HTML(fd)

            fd.write("Prediction model info<pre>\n")
            pprint(self.specD['info'],fd)
            fd.write("</pre>\n")

            fd.write("</body> \n</html>\n")
            
        #os.chmod(outF,0o764) # a+r 
        os.chmod(outF,0o644) # -rw-r--r-- 

    #---------------------
    def print_recom_table(self,fd):
        fd.write('<p>\n Table 1. Predictions for known unambiguous pairs for %s (given %s) <br> <table border="0" >  \n'%(self.name,self.given))
        fd.write('\n   <tr> <th> idx <th width="250"> %s <th width="250"> true %s <th width="180">  predicted <br>true %s rank  <th width="180"> offered <br> %s choices \n'%(self.given,self.recom,self.recom,self.recom))

        out2d=self.specD['matrix']
        irr=0
        for giv in out2d:
            irr+=1
            try:
                trueHK=out2d[giv]['truth']['pair']
            except:
                continue
            
            hk_cnt=self.specD['HK']
            rr_cnt=self.specD['RR']
            offer_cnt=self.specD[self.recom]
            #print('ee',giv,out2d[giv]['truth'])
            rankTrue=out2d[giv]['truth']['rank']
            
            fd.write('<tr align="center"> <td> %d  <td> %s  <td> %s  <td> %s <td> %d\n'%(irr,giv,trueHK,rankTrue,offer_cnt))
            if self.venue!='web':
                print('%s & %s & %d \\\\'%(giv.replace('_','\_'),trueHK.replace('_','\_'),rankTrue))

        fd.write('\n   </table>  \n')
        
 
    #---------------------
    def addAllPlots2HTML(self,fd):
        figCaptL=['0-none','all predicted known HK-RR pairs given %s'%self.given,
                  'Scores for labeled and unkown pairs.']
        k=0

        fd.write('\n <hr>  <table border="0" >  <tr>\n')

        for name in sorted(self.pltName1):
            k+=1
            txt7all='\n  <td  align="center"> <img src="%s.%s" /> <br>Fig.%d  %s<hr>  '%( name,self.imgForm,k,figCaptL[k])
            fd.write(txt7all)
        fd.write('\n  </table > \n')

        k+=1
        name=self.plt2DtopK
        txtHtml='\n  <p> <img src="%s.%s" /> <br>Fig.%d.  %s<hr>  '%( name,self.imgForm,k,'TopN rank over all %s for each %s.'%(self.given,self.recom))
        fd.write(txtHtml)

        k+=1
        name=self.pltPopular
        txtHtml='\n  <p> <img src="%s.%s" /> <br>Fig.%d.  %s<hr>  '%( name,self.imgForm,k,'%s popularity. Dashed lines - respective random choice expectations.'%self.recom)
        fd.write(txtHtml)
        
        fd.write('\n<p>Table 2. HK-RR ranking plots for all %d %s , species: <b> %s </b> '%(self.specD[self.given],self.given ,self.name))
        k2=100
        fd.write('\n <hr>  <table  border="0">\n')
        for name in self.pltName2:            
            if k2%4 ==0:   fd.write('\n <tr> \n')
            k2+=1

            txt7all='\n  <td align="center"> <img src="%s.%s" />   %s<hr>  '%( name,self.imgForm,name)
            fd.write(txt7all)

        fd.write('\n </table >\n')

 
#...!...!..................
    def study_one_zachQ1(self,RRname):
        print('study_one:',RRname)
        out2d=self.specD['matrix']
 
        self.doRRHKplots=False
        giv=RRname
        trueMatch=out2d[giv]['truth']['pair']
        print('trueMatch=',trueMatch)
        self.popular={}# for popularity plot, not relevant
        self.trueRankL=[]
        probV,nameV =self.recom_one( giv,trueMatch,out2d[giv])
        print('got_best prob=%.5f'%probV[0], nameV[0],self.name)
        

